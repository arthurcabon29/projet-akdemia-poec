# Projet Akdemia POEC


## Collaborateurs

- [ ] [Arthur CABON](https://gitlab.com/arthurcabon29)
- [ ] Christelle KOZAILY
- [ ] Erwann LE CORRE

## Contexte

Ce projet a été réalisé dans le cadre d'une formation Java/Angular réalisé du 20/12/2023 au 18/03/2024.

```
Il a été réalisé avec les framework Angular, Spring Boot et Spring Data.
```

## Organisme de formation
[ib cegos](https://www.ib-formation.fr/agences/agence-de-rennes)


## Enoncé

Le projet IT-Training vise à refondre intégralement le système d'information et le portail web de l'organisme de formation du même nom. L'objectif principal est d'améliorer l'efficacité et la convivialité de l'ensemble. Le système complexe englobe un catalogue de plus de 800 formations, un processus d'enregistrement en ligne, une logistique pour l'organisation des sessions, une évaluation détaillée des formations, un service de monitoring, et un volet commercial et SAV. Le délai imparti pour cette refonte est de quatre mois, avec l'objectif de mettre en pratique les compétences des participants dans le développement tout en respectant les contraintes énoncées.

***

# Déploiement

Front
- [ ] NodeJs version 18.19.1 
- [ ] Angular version 16.2 
Back
- [ ] JDK 17.0.6 

Notice de déploiement :
```
 - Créez les nouvelles bases de données «akdemia-gp4e-db» et «akdemia-gp4e-db-test »  
 - Importez les scripts SQL « akdemia-db.sql » et « akdemiae-db-test.sql » respectivement dans les bases «akdemia-gp4e-db» et «akdemia-gp4e-db-test» 

 - Installez les dépendances Maven dans les projets buisness et ws (mvn clean install)
 - Lancer le projet ws en tant Que "Projet Java" (lancer au niveau du fichier AkdemiaWsGp4eApplication.java)

 - Installer les dépendances angular du projet web et démarrer le projet ("npm install" puis "ng serve")
 - Connexion :
     Email : johndoe@gmail.com & Mot de passe : 1234 

```