package af.cmr.indyli.akdemia.business.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.nio.file.AccessDeniedException;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import af.cmr.indyli.akdemia.business.config.AkdemiaBusinessGp4eConfig;
import af.cmr.indyli.akdemia.business.dto.basic.SessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.SessionFullDTO;
import af.cmr.indyli.akdemia.business.dto.full.TopicFullDTO;
import af.cmr.indyli.akdemia.business.exception.AkdemiaBusinessException;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;
import jakarta.annotation.Resource;

@ContextConfiguration(classes = { AkdemiaBusinessGp4eConfig.class })
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class SessionServiceTest {
	
	@Resource(name = ConstsValues.ServiceKeys.SESSION_SERVICE_KEY)
	private ISessionService sessionService;
	private SessionFullDTO sessionForAllTest = null;

	private Integer idCreatedSession = null;

	@BeforeEach
	void setUp() throws AkdemiaBusinessException {
		SessionFullDTO session = getSampleSession();
		
		this.sessionForAllTest = this.sessionService.create(session);

		System.out.println("ID CREATE... " + sessionForAllTest.getId());

		assertNotNull(session);
	}
	

	@Test
	void testCreate() throws AkdemiaBusinessException {
		SessionFullDTO session = getSampleSession();
		session = this.sessionService.create(session);
		idCreatedSession = session.getId();

		assertNotNull(session);
	}
	
	@Test
	void testUpdate() throws AkdemiaBusinessException, AccessDeniedException {
		SessionFullDTO sessionToUpdate = getSampleSession();
		String updateName = "Voila voila...C'est modifié...";
		sessionToUpdate.setId(this.sessionForAllTest.getId());
		sessionToUpdate.setDescription(updateName);

		SessionFullDTO updatedSession = this.sessionService.update(sessionToUpdate);
		assertEquals(updateName, updatedSession.getDescription());
	}
	
	@Test
	void testDelete() throws AkdemiaBusinessException, AccessDeniedException {
		this.sessionService.deleteById(this.sessionForAllTest.getId());
		assertNull(this.sessionService.findById(this.sessionForAllTest.getId()));
		sessionForAllTest = null;
	}

	@Test
	void testFindAll() {
		List<SessionBasicDTO> sessions = this.sessionService.findAll();

//		for(SessionBasicDTO session : sessions) {
//			System.out.println(session.getCode());
//		}
		
		assertEquals(1, sessions.size());
	}
	
	@AfterEach
	void rollback() throws AkdemiaBusinessException, AccessDeniedException {
		if (sessionForAllTest != null)
			this.sessionService.deleteById(this.sessionForAllTest.getId());
		if (idCreatedSession != null)
			this.sessionService.deleteById(idCreatedSession);
	}
	
	
	SessionFullDTO getSampleSession() {
		SessionFullDTO session = new SessionFullDTO();
		session.setCode("0123");
		session.setDuration(3L);
		session.setPrice(2000);
		session.setDescription("test");
		session.setStatus("WAITING");
		session.setDate(new Date(System.currentTimeMillis()+200000));
		session.setLacation("Quelque part");
//		session.setUpdateDate(null);
//		session.setTrainer();
//		session.setTraining();
		
		return session;
	}


}
