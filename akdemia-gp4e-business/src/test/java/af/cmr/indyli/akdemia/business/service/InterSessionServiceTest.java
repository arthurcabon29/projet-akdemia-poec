package af.cmr.indyli.akdemia.business.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.nio.file.AccessDeniedException;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import af.cmr.indyli.akdemia.business.config.AkdemiaBusinessGp4eConfig;
import af.cmr.indyli.akdemia.business.dto.basic.InterSessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.InterSessionFullDTO;
import af.cmr.indyli.akdemia.business.exception.AkdemiaBusinessException;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;
import jakarta.annotation.Resource;

@ContextConfiguration(classes = { AkdemiaBusinessGp4eConfig.class })
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class InterSessionServiceTest {
	
	@Resource(name = ConstsValues.ServiceKeys.INTER_SERVICE_KEY)
	private IInterSessionService interSessionService;
	private InterSessionFullDTO interSessionForAllTest = null;

	private Integer idCreatedInterSession = null;

	@BeforeEach
	void setUp() throws AkdemiaBusinessException {
		InterSessionFullDTO interSession = getSampleInterSession();
		
		this.interSessionForAllTest = this.interSessionService.create(interSession);

		System.out.println("ID CREATE... " + interSessionForAllTest.getId());

		assertNotNull(interSession);
	}
	

	@Test
	void testCreate() throws AkdemiaBusinessException {
		InterSessionFullDTO interSession = getSampleInterSession();
		interSession = this.interSessionService.create(interSession);
		idCreatedInterSession = interSession.getId();

		assertNotNull(interSession);
	}
	
	@Test
	void testUpdate() throws AkdemiaBusinessException, AccessDeniedException {
		InterSessionFullDTO interSessionToUpdate = getSampleInterSession();
		String updateName = "Voila voila...C'est modifié...";
		interSessionToUpdate.setId(this.interSessionForAllTest.getId());
		interSessionToUpdate.setDescription(updateName);

		InterSessionFullDTO updatedInterSession = this.interSessionService.update(interSessionToUpdate);
		assertEquals(updateName, updatedInterSession.getDescription());
	}
	
	@Test
	void testDelete() throws AkdemiaBusinessException, AccessDeniedException {
		this.interSessionService.deleteById(this.interSessionForAllTest.getId());
		assertNull(this.interSessionService.findById(this.interSessionForAllTest.getId()));
		interSessionForAllTest = null;
	}

	@Test
	void testFindAll() {
		List<InterSessionBasicDTO> interSessions = this.interSessionService.findAll();

//		for(SessionBasicDTO interSession : interSessions) {
//			System.out.println(interSession.getCode());
//		}
		
		assertEquals(1, interSessions.size());
	}
	
	@AfterEach
	void rollback() throws AkdemiaBusinessException, AccessDeniedException {
		if (interSessionForAllTest != null)
			this.interSessionService.deleteById(this.interSessionForAllTest.getId());
		if (idCreatedInterSession != null)
			this.interSessionService.deleteById(idCreatedInterSession);
	}
	
	
	InterSessionFullDTO getSampleInterSession() {
		InterSessionFullDTO interSession = new InterSessionFullDTO();
		interSession.setCode("0123");
		interSession.setDuration(3L);
		interSession.setPrice(2000);
		interSession.setDescription("test");
		interSession.setStatus("WAITING");
		interSession.setDate(new Date(System.currentTimeMillis()+200000));
		interSession.setLacation("Quelque part");
//		interSession.setUpdateDate(null);
//		interSession.setTrainer();
//		interSession.setTraining();
		interSession.setMinParticipant(3);
		
		return interSession;
	}
}
