package af.cmr.indyli.akdemia.business.service;

import af.cmr.indyli.akdemia.business.config.AkdemiaBusinessGp4eConfig;
import af.cmr.indyli.akdemia.business.dto.basic.TrainerBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.SessionFullDTO;
import af.cmr.indyli.akdemia.business.dto.full.TrainerFullDTO;
import af.cmr.indyli.akdemia.business.exception.AkdemiaBusinessException;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import java.nio.file.AccessDeniedException;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ContextConfiguration(classes = { AkdemiaBusinessGp4eConfig.class })
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class TrainerServiceTest {

	@Resource(name = ConstsValues.ServiceKeys.TRAINER_SERVICE_KEY)
	private ITrainerService trainerService;
	
//	@Resource(name = ConstsValues.ServiceKeys.VALIDATION_TEST_SERVICE_KEY)
//	private IValidationTestService validationTestService;
	
//	@Resource(name = ConstsValues.ServiceKeys.SESSION_SERVICE_KEY)
//	private ISessionService sessionService;

	private TrainerFullDTO trainerForAllTest = null;
//	private ValidationTestFullDTO validationTestForAllTest = null;
//	private SessionFullDTO sessionTestForAllTest = null;
	private Integer idCreatedEmpl = null;

	@BeforeEach
	void setUp() throws AkdemiaBusinessException {
//		SessionFullDTO session = getSampleSession();
//		this.sessionTestForAllTest = this.sessionService.create(session);
//		System.out.println("session id" + session.getCode());
		TrainerFullDTO trainer = getSampleTrainer();
		this.trainerForAllTest = this.trainerService.create(trainer);
		assertNotNull(trainer);
	}

	@Test
	void testCreate() throws AkdemiaBusinessException {
		TrainerFullDTO trainer = getSampleTrainer();
		trainer.setEmail("nesk@mail.lss");
		trainer = this.trainerService.create(trainer);
		idCreatedEmpl = trainer.getId();
		System.out.println("idCreatedEmpl" + " " + idCreatedEmpl);
		assertNotNull(trainer);
	}

	@Test
	void testFindAll() {
		List<TrainerBasicDTO> trainers = this.trainerService.findAll();
//		System.out.println("first trainer activity" + " " + trainers.get(0).getActivity());
		assertEquals(1, trainers.size());
	}

	@Test
	void testFindById() throws AkdemiaBusinessException {
		TrainerFullDTO trainer = this.trainerService.findById(this.trainerForAllTest.getId());
		assertNotNull(trainer);
		assertEquals(trainer.getAddress(), this.trainerForAllTest.getAddress());
	}

	@Test
	void testUpdate() throws AkdemiaBusinessException, AccessDeniedException {
		TrainerFullDTO trainerToUpdate = getSampleTrainer();
		String updateAddr = "Updated Addr";
		String updateActivity = "Dev fullstack";
		trainerToUpdate.setId(this.trainerForAllTest.getId());
		trainerToUpdate.setAddress(updateAddr);
		trainerToUpdate.setActivity(updateActivity);

		TrainerFullDTO updatedTrainer = this.trainerService.update(trainerToUpdate);
		assertEquals(updateAddr, updatedTrainer.getAddress());
	}

	@Test
	void testDelete() throws AkdemiaBusinessException, AccessDeniedException {
		this.trainerService.deleteById(this.trainerForAllTest.getId());

		assertNull(this.trainerService.findById(this.trainerForAllTest.getId()));
		trainerForAllTest = null;
	}

	@AfterEach
	void rollback() throws AkdemiaBusinessException, AccessDeniedException {
		if (trainerForAllTest != null)
			this.trainerService.deleteById(this.trainerForAllTest.getId());
		if (idCreatedEmpl != null)
			this.trainerService.deleteById(idCreatedEmpl);
//		if (sessionTestForAllTest != null)
//			this.sessionService.deleteById(this.sessionTestForAllTest.getId());
	}
	
//	SessionFullDTO getSampleSession() {
//		SessionFullDTO session = new SessionFullDTO();
//		session.setCode("0123");
//		session.setDuration(3L);
//		session.setPrice(2000);
//		session.setDescription("test");
//		session.setStatus("WAITING");
//		session.setDate(new Date(System.currentTimeMillis()+200000));
//		session.setLacation("Quelque part");
////		session.setUpdateDate(null);
////		session.setTrainer();
////		session.setTraining();
//		
//		return session;
//	}


	TrainerFullDTO getSampleTrainer() {
		TrainerFullDTO trainer = new TrainerFullDTO();
		trainer.setAddress("DLA");
		trainer.setEmail("mail@trainer.com");
		trainer.setFirstname("Lionel");
		trainer.setLastname("Messi");
		trainer.setLogin("scplog");
		trainer.setPassword("pass123");
		trainer.setPhone("08765678900");
		trainer.setPhoto("/profil/trainer.jpg");
		trainer.setActivity("C-Activity");
		trainer.setCvLink("https://www.sample.com/cv");
		trainer.setGender("M");
		trainer.setCreationDate(new Date());
		return trainer;
	}

}
