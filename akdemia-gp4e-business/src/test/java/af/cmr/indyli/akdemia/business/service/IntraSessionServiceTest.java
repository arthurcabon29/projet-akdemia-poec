package af.cmr.indyli.akdemia.business.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.nio.file.AccessDeniedException;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import af.cmr.indyli.akdemia.business.config.AkdemiaBusinessGp4eConfig;
import af.cmr.indyli.akdemia.business.dto.basic.CompanyBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.IntraSessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.CompanyFullDTO;
import af.cmr.indyli.akdemia.business.dto.full.IntraSessionFullDTO;
import af.cmr.indyli.akdemia.business.exception.AkdemiaBusinessException;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;
import jakarta.annotation.Resource;

@ContextConfiguration(classes = { AkdemiaBusinessGp4eConfig.class })
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class IntraSessionServiceTest {
	
	@Resource(name = ConstsValues.ServiceKeys.INTRA_SERVICE_KEY)
	private IIntraSessionService intraSessionService;
	private IntraSessionFullDTO intraSessionForAllTest = null;

	private Integer idCreatedIntraSession = null;

	@BeforeEach
	void setUp() throws AkdemiaBusinessException {
		IntraSessionFullDTO intraSession = getSampleIntraSession();
		
		this.intraSessionForAllTest = this.intraSessionService.create(intraSession);

		System.out.println("ID CREATE... " + intraSessionForAllTest.getId());

		assertNotNull(intraSession);
	}
	

	@Test
	void testCreate() throws AkdemiaBusinessException {
		IntraSessionFullDTO intraSession = getSampleIntraSession();
		intraSession = this.intraSessionService.create(intraSession);
		idCreatedIntraSession = intraSession.getId();

		assertNotNull(intraSession);
	}
	
	@Test
	void testUpdate() throws AkdemiaBusinessException, AccessDeniedException {
		IntraSessionFullDTO intraSessionToUpdate = getSampleIntraSession();
		String updateName = "Voila voila...C'est modifié...";
		intraSessionToUpdate.setId(this.intraSessionForAllTest.getId());
		intraSessionToUpdate.setDescription(updateName);

		IntraSessionFullDTO updatedIntraSession = this.intraSessionService.update(intraSessionToUpdate);
		assertEquals(updateName, updatedIntraSession.getDescription());
	}
	
	@Test
	void testDelete() throws AkdemiaBusinessException, AccessDeniedException {
		this.intraSessionService.deleteById(this.intraSessionForAllTest.getId());
		assertNull(this.intraSessionService.findById(this.intraSessionForAllTest.getId()));
		intraSessionForAllTest = null;
	}

	@Test
	void testFindAll() {
		List<IntraSessionBasicDTO> intraSessions = this.intraSessionService.findAll();

//		for(SessionBasicDTO intraSession : intraSessions) {
//			System.out.println(intraSession.getCode());
//		}
		
		assertEquals(1, intraSessions.size());
	}
	
	@AfterEach
	void rollback() throws AkdemiaBusinessException, AccessDeniedException {
		if (intraSessionForAllTest != null)
			this.intraSessionService.deleteById(this.intraSessionForAllTest.getId());
		if (idCreatedIntraSession != null)
			this.intraSessionService.deleteById(idCreatedIntraSession);
	}
	
	
	IntraSessionFullDTO getSampleIntraSession() {
		IntraSessionFullDTO intraSession = new IntraSessionFullDTO();
		intraSession.setCode("0123");
		intraSession.setDuration(3L);
		intraSession.setPrice(2000);
		intraSession.setDescription("test");
		intraSession.setStatus("WAITING");
		intraSession.setDate(new Date(System.currentTimeMillis()+200000));
		intraSession.setLacation("Quelque part");
		//		intraSession.setUpdateDate(null);
//		intraSession.setTrainer();
//		intraSession.setTraining();
		
		
		//création company pour le test
//		CompanyBasicDTO company = new CompanyBasicDTO();
//		company.setAddress("DLA");
//		company.setEmail("mail@company.com");
//		company.setName("SCP");
//		company.setLogin("scplog");
//		company.setPassword("pass123");
//		company.setPhone("08765678900");
//		company.setPhoto("/profil/company.jpg");
//		company.setActivity("C-Activity");
//		company.setCreationDate(new Date());
//		intraSession.setCompany(company);
		
		return intraSession;
	}
}
