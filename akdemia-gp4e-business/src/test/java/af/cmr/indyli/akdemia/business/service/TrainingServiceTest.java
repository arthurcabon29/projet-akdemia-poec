package af.cmr.indyli.akdemia.business.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.nio.file.AccessDeniedException;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import af.cmr.indyli.akdemia.business.config.AkdemiaBusinessGp4eConfig;
import af.cmr.indyli.akdemia.business.dto.basic.ManagerBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.TrainingBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.ManagerFullDTO;
import af.cmr.indyli.akdemia.business.dto.full.TopicFullDTO;
import af.cmr.indyli.akdemia.business.dto.full.TrainingFullDTO;
import af.cmr.indyli.akdemia.business.exception.AkdemiaBusinessException;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;
import jakarta.annotation.Resource;

@ContextConfiguration(classes = { AkdemiaBusinessGp4eConfig.class })
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class TrainingServiceTest {
	@Resource(name = ConstsValues.ServiceKeys.TRAINING_SERVICE_KEY)
	private ITrainingService trainingService;
	private TrainingFullDTO trainingForAllTest = null;
	private Integer idCreatedTraining = null;
	
	@BeforeEach
	void setUp() throws AkdemiaBusinessException {
		TrainingFullDTO training = getSampleTraining();
	//	training = this.trainingService.create(training);

		this.trainingForAllTest = this.trainingService.create(training);
		System.out.println("ID CREATE... " + trainingForAllTest.getId());
		assertNotNull(training);
	}
	
	
    @Test 
	void testCreate() throws AkdemiaBusinessException {
		TrainingFullDTO training = getSampleTraining();
	    training.setTrainingTitle("Informatique");
	    training = this.trainingService.create(training);
		idCreatedTraining = training.getId();

		assertNotNull(training);
	}

	
//	TrainingFullDTO getSampleTraining() {
//		TrainingFullDTO training = new TrainingFullDTO();
//        training.setTrainingTitle("Title2");
//        training.setTrainingDescription("blabla");
//        training.setTrainingPrice(2000);
//        training.setCreationDate(new Date());
//
//		return training;
//	}
//	
	@Test
	void testFindAll() {
		List<TrainingBasicDTO> trainings = this.trainingService.findAll();

		assertEquals(1, trainings.size());
	}
	
	@Test
	void testFindById() throws AkdemiaBusinessException {
		TrainingFullDTO training = this.trainingService.findById(this.trainingForAllTest.getId());
		assertNotNull(training);
		assertEquals(training.getTrainingTitle(), this.trainingForAllTest.getTrainingTitle());
	}
	
	
	@Test
	void testDelete() throws AkdemiaBusinessException, AccessDeniedException {
		this.trainingService.deleteById(this.trainingForAllTest.getId());

		assertNull(this.trainingService.findById(this.trainingForAllTest.getId()));
		trainingForAllTest = null; 
	}
	
	
	@Test
	void testUpdate() throws AkdemiaBusinessException, AccessDeniedException {
		TrainingFullDTO trainingToUpdate = getSampleTraining();
		String updatetitle = "Updated title";
		trainingToUpdate.setId(this.trainingForAllTest.getId());
		trainingToUpdate.setTrainingTitle(updatetitle);

		TrainingFullDTO updatedTraining = this.trainingService.update(trainingToUpdate);
		assertEquals(updatetitle, updatedTraining.getTrainingTitle());
	}
	
	@AfterEach
	void rollback() throws AkdemiaBusinessException, AccessDeniedException {
		if (trainingForAllTest != null)
			this.trainingService.deleteById(this.trainingForAllTest.getId());
		if (idCreatedTraining != null)
			this.trainingService.deleteById(idCreatedTraining);
	}

	TrainingFullDTO getSampleTraining() {
		TrainingFullDTO training = new TrainingFullDTO();
		training.setTrainingTitle("Example Training");
		training.setTrainingDescription("Example training description");
		training.setLogo("logo'importequoi");
		training.setCreationDate(new Date());

		return training;
	}

}
