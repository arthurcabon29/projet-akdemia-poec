package af.cmr.indyli.akdemia.business.service.impl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import af.cmr.indyli.akdemia.business.dao.IEmployeeSouscriptionRepository;
import af.cmr.indyli.akdemia.business.dto.basic.EmployeeBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.EmployeeSouscriptionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.ParticularBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.SessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.EmployeeSouscriptionFullDTO;
import af.cmr.indyli.akdemia.business.dto.full.SessionFullDTO;
import af.cmr.indyli.akdemia.business.entity.Employee;
import af.cmr.indyli.akdemia.business.entity.EmployeeSouscription;
import af.cmr.indyli.akdemia.business.entity.Particular;
import af.cmr.indyli.akdemia.business.service.IEmployeeSouscriptionService;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;
import jakarta.annotation.Resource;

/**
 * Service implementation class for managing {@link EmployeeSouscription} entity, extending the
 * AbstractAkdemiaServiceImpl class. This class provides specific functionality
 * for managing EmployeeSouscription, including CRUD operations.
 *
 * @see AbstractAkdemiaServiceImpl
 */
@Service(ConstsValues.ServiceKeys.ESOUSCRIPTION_SERVICE_KEY)
public class EmployeeSouscriptionServiceImpl extends AbstractAkdemiaServiceImpl<EmployeeSouscription, EmployeeSouscriptionBasicDTO, EmployeeSouscriptionFullDTO, IEmployeeSouscriptionRepository> implements IEmployeeSouscriptionService  {


	@Resource(name = ConstsValues.ConstsDAO.ESOUSCRIPTION_DAO_KEY)
	private IEmployeeSouscriptionRepository employeeSouscriptionRepository;
	 
	private final ModelMapper modelMapper;
	 
	 
	@Autowired
	public EmployeeSouscriptionServiceImpl(IEmployeeSouscriptionRepository employeeSouscriptionRepository, ModelMapper modelMapper) {
	    super(EmployeeSouscription.class, EmployeeSouscriptionBasicDTO.class, EmployeeSouscriptionFullDTO.class);
	    this.employeeSouscriptionRepository = employeeSouscriptionRepository;
	    this.modelMapper = modelMapper;
	}
 

	@Override
	public EmployeeSouscriptionFullDTO create(EmployeeSouscriptionFullDTO ent) {
		ent.setCreationDate(new Date());
		EmployeeSouscription entity = this.getDAO().saveAndFlush(this.getModelMapper().map(ent, EmployeeSouscription.class));
		return this.getModelMapper().map(entity, EmployeeSouscriptionFullDTO.class);
	}
	
	@Override
	public IEmployeeSouscriptionRepository getDAO() {
	    return this.employeeSouscriptionRepository;
	}
	
	@Override
	public List<EmployeeSouscriptionBasicDTO> findAllEmployeeSouscription() {
		List<EmployeeSouscription> employeeSouscriptions = getDAO().findAll();
	    return employeeSouscriptions.stream()
	            .map(employeeSouscription -> modelMapper.map(employeeSouscription, EmployeeSouscriptionBasicDTO.class))
	            .collect(Collectors.toList());
	}


	@Override
	public List<EmployeeBasicDTO> findAllEmployeeSouscriptionByIntra(int id) {
		List<Employee> employees = getDAO().findAllbyIdIntra(id);
	    return employees.stream()
	            .map(particularSouscription -> modelMapper.map(particularSouscription, EmployeeBasicDTO.class))
	            .collect(Collectors.toList());
	}

}
