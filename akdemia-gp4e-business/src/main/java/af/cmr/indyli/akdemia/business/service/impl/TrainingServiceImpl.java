package af.cmr.indyli.akdemia.business.service.impl;


import java.nio.file.AccessDeniedException;
import java.util.Date;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;


import af.cmr.indyli.akdemia.business.dao.ITrainingRepository;

import af.cmr.indyli.akdemia.business.dto.basic.TrainingBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.EmployeeFullDTO;
import af.cmr.indyli.akdemia.business.dto.full.ManagerFullDTO;
import af.cmr.indyli.akdemia.business.dto.full.TopicFullDTO;
import af.cmr.indyli.akdemia.business.dto.full.TrainingFullDTO;
import af.cmr.indyli.akdemia.business.entity.Company;
import af.cmr.indyli.akdemia.business.entity.Employee;
import af.cmr.indyli.akdemia.business.entity.Manager;
import af.cmr.indyli.akdemia.business.entity.Topic;
import af.cmr.indyli.akdemia.business.entity.Training;
import af.cmr.indyli.akdemia.business.exception.AkdemiaBusinessException;
import af.cmr.indyli.akdemia.business.service.ITrainingService;
import af.cmr.indyli.akdemia.business.service.IUserService;
import af.cmr.indyli.akdemia.business.utils.ConstBusinessRules;
import af.cmr.indyli.akdemia.business.utils.ConstRejectBusinessMessage;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;
import jakarta.annotation.Resource;

@Service(ConstsValues.ServiceKeys.TRAINING_SERVICE_KEY)
public class TrainingServiceImpl extends AbstractAkdemiaServiceImpl< Training, TrainingBasicDTO, TrainingFullDTO, ITrainingRepository> 
    implements ITrainingService {

	@Resource(name = ConstsValues.ConstsDAO.TRAINING_DAO_KEY)
	private ITrainingRepository trainingRepository;
	//@Resource(name = ConstsValues.ServiceKeys.TRAINING_SERVICE_KEY)
	//private ITrainingService trainingService;

	public TrainingServiceImpl() {
		super(Training.class, TrainingBasicDTO.class, TrainingFullDTO.class);
	}
	
	@Override
	public ITrainingRepository getDAO() {
		
		return this.trainingRepository;
	}

	
	@Override
	public TrainingFullDTO create(TrainingFullDTO view) throws AkdemiaBusinessException {
		Training training = this.getDAO().findByTrainingTitle(view.getTrainingTitle());
		if (training == null) {
			view.setCreationDate(new Date());
			Training entity = this.getDAO().saveAndFlush(this.getModelMapper().map(view, Training.class));
			view.setId(entity.getId());
			return view;
		}
		// Le titre d'une formation est unique
		throw new AkdemiaBusinessException(ConstBusinessRules.RG04);
	}

	@Override
	public TrainingFullDTO update(TrainingFullDTO viewToUpdate) throws AkdemiaBusinessException, AccessDeniedException {
		boolean isTrainingExist = this.findAll().stream().anyMatch(
				p -> viewToUpdate.getTrainingTitle().equals(p.getTrainingTitle()) && !viewToUpdate.getId().equals(p.getId()));
		if (!isTrainingExist) {
			viewToUpdate.setUpdateDate(new Date());
			Training entity = this.getDAO().findById(viewToUpdate.getId()).orElse(null);
			if (entity != null) {
				BeanUtils.copyProperties(viewToUpdate, entity);
				this.getDAO().saveAndFlush(entity);
			} else {
				throw new AkdemiaBusinessException(ConstRejectBusinessMessage.UPDATE_OBJECT_NOT_FOUND);
			}
			return viewToUpdate;
		}
		//Christelle: not sure which regle de gestion d'utiliser ici
		throw new AkdemiaBusinessException(ConstBusinessRules.RG04);
	}
	

//    @Override
//	public void deleteById(int id) throws AkdemiaBusinessException, AccessDeniedException {
//		var trainingToRemove = this.findById(id);

//		if (trainingToRemove == null) {
//			throw new AkdemiaBusinessException(ConstRejectBusinessMessage.DELETE_OBJECT_NOT_FOUND);
//		}

//		getDAO().deleteById(id);
//	}
    
    @Override
	public void deleteById(int id) throws AkdemiaBusinessException, AccessDeniedException {
		var tmpSession = this.findById(id);

		if (tmpSession == null) {
			throw new AkdemiaBusinessException(ConstRejectBusinessMessage.DELETE_OBJECT_NOT_FOUND);
		}

		if (tmpSession.getSessions().isEmpty()) {
		this.getDAO().deleteById(id);
		} else {
			// Une formation étant planifiée sur au moins une session ne doit pas être supprimée du système 
			throw new AkdemiaBusinessException(ConstBusinessRules.RG18);
		}
	}



	//public TrainingFullDTO findTrainingById(Integer idTraining) throws AkdemiaBusinessException {
	//	Training training = trainingRepository.findById(idTraining).orElse(null);
	//	if (training == null)
	//		throw new AkdemiaBusinessException(ConstRejectBusinessMessage.OBJECT_SEARCHED_NOT_FOUND);
	//	return getModelMapper().map(getDAO().findById(idTraining), TrainingFullDTO.class);}
	
//	public TrainingFullDTO findTrainingByTitle(String title) throws AkdemiaBusinessException {
//		Training training = trainingRepository.findByTrainingTitle(title);
//		if (training == null)
//			throw new AkdemiaBusinessException(ConstRejectBusinessMessage.OBJECT_SEARCHED_NOT_FOUND);
//		return getModelMapper().map(getDAO().findByTrainingTitle(title), TrainingFullDTO.class);
//	}
	
 //   @Override
//	public TrainingFullDTO create(TrainingFullDTO trainingNew) throws AkdemiaBusinessException {
	//	if (findByTrainingTitle(trainingNew.getTrainingTitle()) == null) {
	//		Training training = this.getDAO().saveAndFlush(this.getModelMapper().map(trainingNew, Training.class));

//			return this.getModelMapper().map(training, TrainingFullDTO.class);
//		} else {
		    // Training with the specified title already exists
//		    throw new AkdemiaBusinessException(ConstBusinessRules.RG04);
//		}
			
//	}
		
	//	public TrainingFullDTO createI(TrainingFullDTO trainingNew) throws AkdemiaBusinessException {
	//		if (findTrainingById(trainingNew.getId()) == null) {
	//			Training training = this.getDAO().saveAndFlush(this.getModelMapper().map(trainingNew, Training.class));

	//			return this.getModelMapper().map(training, TrainingFullDTO.class);
	//		} else {
	//		    // Training with the specified title already exists
	//		    throw new AkdemiaBusinessException(ConstBusinessRules.RG04);
	//		}}
		
	//	@Override
	//	public TrainingFullDTO update(TrainingFullDTO trainingToUpdate) throws AkdemiaBusinessException, AccessDeniedException {
	//		if (findTrainingById(trainingToUpdate.getId()) != null) {
	//			trainingToUpdate.setUpdateDate(new Date());
	//			Training entity = this.getDAO().findById( trainingToUpdate.getId()).orElse(null);
	//			if (entity != null) {
	//				BeanUtils.copyProperties( trainingToUpdate, entity);
	//				this.getDAO().saveAndFlush(entity);
	//			} else {
	//				throw new AkdemiaBusinessException(ConstRejectBusinessMessage.UPDATE_OBJECT_NOT_FOUND);
	//			}
	//			return trainingToUpdate;
	//		}
	//		throw new AkdemiaBusinessException(ConstBusinessRules.RG04);
	//	}
	
	
	
	

}
