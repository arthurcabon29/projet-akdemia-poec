package af.cmr.indyli.akdemia.business.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import af.cmr.indyli.akdemia.business.entity.InterSession;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;

/**
 * This interface serves as a repository for managing {@link InterSession} entities
 * in the database. It extends JpaRepository, providing CRUD operations for the
 * Session entity with Integer as the type of its primary key.
 */
@Repository(value = ConstsValues.ConstsDAO.INTER_DAO_KEY)
public interface IInterSessionRepository extends JpaRepository<InterSession, Integer> {
	List<InterSession> findAll();
}
