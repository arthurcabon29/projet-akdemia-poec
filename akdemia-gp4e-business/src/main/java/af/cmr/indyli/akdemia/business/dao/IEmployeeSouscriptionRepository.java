package af.cmr.indyli.akdemia.business.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import af.cmr.indyli.akdemia.business.entity.EmployeeSouscription;
import af.cmr.indyli.akdemia.business.entity.Employee;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;

/**
 * This interface serves as a repository for managing {@link EmployeeSouscription} entities
 * in the database. It extends JpaRepository, providing CRUD operations for the
 * EmployeeSouscription entity with Integer as the type of its primary key.
 */
@Repository(value = ConstsValues.ConstsDAO.ESOUSCRIPTION_DAO_KEY)
public interface IEmployeeSouscriptionRepository  extends JpaRepository<EmployeeSouscription, Integer> {
	List<EmployeeSouscription> findAll();
	
	@Query("SELECT e FROM EmployeeSouscription es JOIN es.intraSession s JOIN es.employee e WHERE s.id = :idIntraSession")
	List<Employee> findAllbyIdIntra(@Param("idIntraSession") int id);
}
