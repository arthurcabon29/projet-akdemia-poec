package af.cmr.indyli.akdemia.business.dto.full;

import java.util.ArrayList;
import java.util.List;

import af.cmr.indyli.akdemia.business.dto.basic.EmployeeBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.IntraSessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.medium.CompanyMediumDTO;
import af.cmr.indyli.akdemia.business.entity.Company;

/**
 * This class represents a full Data Transfer Object (DTO) for a {@link Company}
 * entity, it extends {@link CompanyMediumDTO}.
 */
public class CompanyFullDTO extends CompanyMediumDTO {
	private List<EmployeeBasicDTO> employees = new ArrayList<>();
	private List<IntraSessionBasicDTO> intraSessions = new ArrayList<>();


	public CompanyFullDTO() {
	}

	public List<EmployeeBasicDTO> getEmployees() {
		return employees;
	}

	public void setEmployees(List<EmployeeBasicDTO> employees) {
		this.employees = employees;
	}
	
	public List<IntraSessionBasicDTO> getIntraSessions() {
		return intraSessions;
	}

	public void setIntraSessions(List<IntraSessionBasicDTO> intraSessions) {
		this.intraSessions = intraSessions;
	}


}
