package af.cmr.indyli.akdemia.business.entity;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

/**
 * This class represents an Employee entity. It encapsulates information about
 * an individual intern within the company.
 */
@Entity
@Table(name = "AKDEMIA_PARTICULAR_SOUSCRIPTION")
public class ParticularSouscription implements IEntity {
	
	@Id
	@Column(name = "ID")
	private Integer id;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "CREATION_DATE")
	private Date creationDate;
	
	@Column(name = "UPDATE_DATE")
	private Date updateDate;
	
	@ManyToOne
    @JoinColumn(name = "ID_AKDEMIA_INTER_SESSION")
    private InterSession interSession;
	
	@ManyToOne
    @JoinColumn(name = "ID_AKDEMIA_PARTICULAR")
    private Particular particular;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public InterSession getInterSession() {
		return interSession;
	}

	public void setInterSession(InterSession interSession) {
		this.interSession = interSession;
	}

	public Particular getParticular() {
		return particular;
	}

	public void setParticular(Particular particular) {
		this.particular = particular;
	}

	

}