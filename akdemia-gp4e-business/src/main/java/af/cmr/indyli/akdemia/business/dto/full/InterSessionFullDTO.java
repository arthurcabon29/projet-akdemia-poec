package af.cmr.indyli.akdemia.business.dto.full;

import java.util.ArrayList;
import java.util.List;

import af.cmr.indyli.akdemia.business.dto.basic.EvaluationBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.ParticularSouscriptionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.medium.InterSessionMediumDTO;
import af.cmr.indyli.akdemia.business.entity.ParticularSouscription;

public class InterSessionFullDTO extends InterSessionMediumDTO {
	private List<EvaluationBasicDTO> evaluations = new ArrayList<>();

	private List<ParticularSouscriptionBasicDTO> ParticularSouscription = new ArrayList<>();
	

	public List<EvaluationBasicDTO> getEvaluation() {
		return evaluations;
	}

	public void setEvaluation(List<EvaluationBasicDTO> evaluations) {
		this.evaluations = evaluations;
	}
	
	public List<ParticularSouscriptionBasicDTO> getParticularSouscription() {
		return ParticularSouscription;
	}

	public void setParticularSouscription(List<ParticularSouscriptionBasicDTO> particularSouscription) {
		ParticularSouscription = particularSouscription;
	}
}
