package af.cmr.indyli.akdemia.business.dto.full;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import af.cmr.indyli.akdemia.business.dto.basic.PlanningBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.SessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.SubTopicBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.TrainingBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.ValidationTestBasicDTO;
import af.cmr.indyli.akdemia.business.dto.medium.TrainingMediumDTO;

public class TrainingFullDTO extends TrainingMediumDTO {
	private List<SessionBasicDTO> sessions = new ArrayList<>();
	public List<SessionBasicDTO> getSessions() {
		return sessions ;
		}
	
    public void setSessions(List<SessionBasicDTO> sessions) {
	this.sessions = sessions;
	}
    
	
	private List<SubTopicBasicDTO> subThemes = new ArrayList<>();

	public List<SubTopicBasicDTO> getSubTopics() {
		return subThemes;
		}

	 public void setSubThemes(List<SubTopicBasicDTO> subThemes) {
		 this.subThemes = subThemes;
		 }
    
}




	
	
		
	
	
	//private List<SubTopicBasicDTO> subThemes = new ArrayList<>();

//	public List<SubTopicBasicDTO> getSubTopics() {	return subThemes;}

	// public void setSubThemes(List<SubTopicBasicDTO> subThemes) {this.subThemes = subThemes;}
	
	
//	private List<ValidationTestBasicDTO> validationTests = new ArrayList<>();
//	public List<ValidationTestBasicDTO> getValidationTests() {
//		return validationTests;	}
	
//	private List<PlanningBasicDTO> planings = new ArrayList<>();
//	public List<PlanningBasicDTO> getPlannings() {	return planings;	}
	
	// private List<SessionBasicDTO> sessions = new ArrayList<>();
	 // public List<SessionBasicDTO> getSessions() {
	// 	return sessions; }
	
	
	
