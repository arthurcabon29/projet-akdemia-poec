package af.cmr.indyli.akdemia.business.dto.medium;

import java.util.Date;

import af.cmr.indyli.akdemia.business.dto.basic.SessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.TrainerBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.TrainingBasicDTO;


/**
 * This class represents a medium Data Transfer Object (DTO) for a
 * {@link Session} entity. It extends {@link SessionMediumDTO}.
 * Medium DTOs typically include additional
 * details beyond the basic DTO but exclude complex associations like lists.
 */
public class SessionMediumDTO extends SessionBasicDTO {

	
    private TrainerBasicDTO trainer;
    private TrainingBasicDTO training;
	


	public TrainerBasicDTO getTrainer() {
		return trainer;
	}

	public void setTrainer(TrainerBasicDTO trainer) {
		this.trainer = trainer;
	}

	public TrainingBasicDTO getTraining() {
		return training;
	}

	public void setTraining(TrainingBasicDTO training) {
		this.training = training;
	}
	
}
