package af.cmr.indyli.akdemia.business.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "AKDEMIA_TRAINING")
public class Training implements IEntity {
	
   

   @Id
   @Column(name = "ID")
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Integer id;
	
   @Column(name = "TITLE")
   private String trainingTitle;
   
   
   
   @Column(name = "DESCRIPTION")
   private String trainingDescription;
   
   @Column(name = "TRAINING_PRICE")
   private double trainingPrice; 
   
   @Column(name = "CREATION_DATE")
   private Date creationDate;
   
   @Column(name = "LOGO")
   private String logo;
   
   @Column(name = "UPDATE_DATE")
   private Date updateDate;
   
   @ManyToOne
	@JoinColumn(name = "ID_AKDEMIA_REQUIREMENT")
	private Requirement requirement;
   
   
   @OneToMany(mappedBy = "training")
	private List<Session> sessions;

   
   @OneToMany(mappedBy = "training")
	private List<Planning> plannings;

   
   @OneToMany(mappedBy = "training")
	private List<ValidationTest> validationTests;

   
   @ManyToMany(fetch = FetchType.EAGER)
   @JoinTable(name = "compose", joinColumns = @JoinColumn(name = "ID"), inverseJoinColumns = @JoinColumn(name = "ID_AKDEMIA_SUB_THEME"))
   private List<SubTopic> subTopics = new ArrayList<>();
   
   public Training () {
	   
   }
   
   public Training(String trainingTitle) {
		this.trainingTitle = trainingTitle; 
		
	}

@Override
public Integer getId() {
	return id;
}

@Override
public void setId(Integer id) {
	this.id = id;
	
}

@Override
public Date getCreationDate() {
		return creationDate;
}

@Override
public void setCreationDate(Date creationDate) {
	this.creationDate = creationDate;
	
}

@Override
public Date getUpdateDate() {
	return updateDate;
}

@Override
public void setUpdateDate(Date updateDate) {
	this.updateDate = updateDate;
	
}

public String getLogo() {
	return logo;
}

public void setLogo(String logo) {
	this.logo = logo;
	
}

public String getTrainingTitle() {
		return trainingTitle;
}


public void setTrainingTitle(String trainingTitle) {
	this.trainingTitle = trainingTitle;
	
}

public String getTrainingDescription() {
	return trainingDescription ;
}

public void setTrainingDescription(String trainingDescription) {
	this.trainingDescription = trainingDescription;
	
}

public double getTrainingPrice() {
	return trainingPrice;
}

public void setTrainingPrice(double trainingPrice) {
	this.trainingPrice = trainingPrice;
	
}


   
}
