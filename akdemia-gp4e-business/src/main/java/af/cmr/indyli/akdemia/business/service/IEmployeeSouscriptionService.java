package af.cmr.indyli.akdemia.business.service;

import java.util.List;

import af.cmr.indyli.akdemia.business.dao.IEmployeeSouscriptionRepository;
import af.cmr.indyli.akdemia.business.dto.basic.EmployeeSouscriptionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.EmployeeBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.EmployeeSouscriptionFullDTO;
import af.cmr.indyli.akdemia.business.entity.EmployeeSouscription;
import af.cmr.indyli.akdemia.business.exception.AkdemiaBusinessException;

/**
 * Interface extending the IAbstractAkdemiaService interface for managing employeeSouscription,
 * providing specific operations for {@link EmployeeSouscription} entity.
 *
 * @see IAbstractAkdemiaService
 */
public interface IEmployeeSouscriptionService 
extends IAbstractAkdemiaService<EmployeeSouscription, EmployeeSouscriptionBasicDTO, EmployeeSouscriptionFullDTO, IEmployeeSouscriptionRepository>{
	List<EmployeeSouscriptionBasicDTO> findAllEmployeeSouscription();

	EmployeeSouscriptionFullDTO create(EmployeeSouscriptionFullDTO ent) throws AkdemiaBusinessException;

	List<EmployeeBasicDTO>  findAllEmployeeSouscriptionByIntra(int id);
}
