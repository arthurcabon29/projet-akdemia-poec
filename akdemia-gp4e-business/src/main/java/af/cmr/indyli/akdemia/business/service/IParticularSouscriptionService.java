package af.cmr.indyli.akdemia.business.service;

import java.util.List;

import af.cmr.indyli.akdemia.business.dao.IParticularSouscriptionRepository;
import af.cmr.indyli.akdemia.business.dto.basic.ParticularBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.ParticularSouscriptionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.ParticularSouscriptionFullDTO;
import af.cmr.indyli.akdemia.business.entity.ParticularSouscription;
import af.cmr.indyli.akdemia.business.exception.AkdemiaBusinessException;

/**
 * Interface extending the IAbstractAkdemiaService interface for managing particularSouscription,
 * providing specific operations for {@link ParticularSouscription} entity.
 *
 * @see IAbstractAkdemiaService
 */
public interface IParticularSouscriptionService 
extends IAbstractAkdemiaService<ParticularSouscription, ParticularSouscriptionBasicDTO, ParticularSouscriptionFullDTO, IParticularSouscriptionRepository>{
	List<ParticularSouscriptionBasicDTO> findAllParticularSouscription();

	ParticularSouscriptionFullDTO create(ParticularSouscriptionFullDTO ent) throws AkdemiaBusinessException;

	List<ParticularBasicDTO>  findAllParticularSouscriptionByInter(int id);
}
