package af.cmr.indyli.akdemia.business.service;

import java.util.List;


import af.cmr.indyli.akdemia.business.dao.ISessionRepository;
import af.cmr.indyli.akdemia.business.dto.basic.SessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.SessionFullDTO;
import af.cmr.indyli.akdemia.business.entity.Session;
import af.cmr.indyli.akdemia.business.exception.AkdemiaBusinessException;

/**
 * Interface extending the IAbstractAkdemiaService interface for managing session,
 * providing specific operations for {@link Session} entity.
 *
 * @see IAbstractAkdemiaService
 */
public interface ISessionService 
extends IAbstractAkdemiaService<Session, SessionBasicDTO, SessionFullDTO, ISessionRepository>{
	List<SessionBasicDTO> findAllSession();

	SessionFullDTO create(SessionFullDTO ent) throws AkdemiaBusinessException;
//	SessionFullDTO findById(int id);
}
