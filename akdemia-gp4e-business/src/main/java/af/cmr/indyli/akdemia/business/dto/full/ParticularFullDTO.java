package af.cmr.indyli.akdemia.business.dto.full;

import java.util.ArrayList;
import java.util.List;

import af.cmr.indyli.akdemia.business.dto.basic.EvaluationBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.ParticularSouscriptionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.medium.ParticularMediumDTO;
import af.cmr.indyli.akdemia.business.entity.Particular;
import af.cmr.indyli.akdemia.business.entity.ParticularSouscription;

/**
 * This class represents a full Data Transfer Object (DTO) for a
 * {@link Particular} entity, it extends {@link ParticularMediumDTO}.
 */
public class ParticularFullDTO extends ParticularMediumDTO {

	private List<ParticularSouscriptionBasicDTO> ParticularSouscription = new ArrayList<>();

	public ParticularFullDTO() {
	}

	public List<ParticularSouscriptionBasicDTO> getParticularSouscription() {
		return ParticularSouscription;
	}

	public void setParticularSouscription(List<ParticularSouscriptionBasicDTO> particularSouscription) {
		ParticularSouscription = particularSouscription;
	}
	
	

}
