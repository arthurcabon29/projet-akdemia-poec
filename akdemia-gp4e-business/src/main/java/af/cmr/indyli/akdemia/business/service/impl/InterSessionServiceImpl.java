package af.cmr.indyli.akdemia.business.service.impl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import af.cmr.indyli.akdemia.business.dao.IInterSessionRepository;
import af.cmr.indyli.akdemia.business.dto.basic.InterSessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.InterSessionFullDTO;
import af.cmr.indyli.akdemia.business.entity.InterSession;
import af.cmr.indyli.akdemia.business.service.IInterSessionService;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;
import jakarta.annotation.Resource;

/**
 * Service implementation class for managing {@link InterSession} entity, extending the
 * AbstractAkdemiaServiceImpl class. This class provides specific functionality
 * for managing InterSession, including CRUD operations.
 *
 * @see AbstractAkdemiaServiceImpl
 */
@Service(ConstsValues.ServiceKeys.INTER_SERVICE_KEY)
public class InterSessionServiceImpl extends AbstractAkdemiaServiceImpl<InterSession, InterSessionBasicDTO, InterSessionFullDTO, IInterSessionRepository> implements IInterSessionService {

	@Resource(name = ConstsValues.ConstsDAO.INTER_DAO_KEY)
	private IInterSessionRepository interSessionRepository;
	 
	private final ModelMapper modelMapper;
	
	@Autowired
	public InterSessionServiceImpl(IInterSessionRepository interSessionRepository, ModelMapper modelMapper) {
	    super(InterSession.class, InterSessionBasicDTO.class, InterSessionFullDTO.class);
	    this.interSessionRepository = interSessionRepository;
	    this.modelMapper = modelMapper;
	}
	
	
	@Override
	public InterSessionFullDTO create(InterSessionFullDTO ent) {
		ent.setCreationDate(new Date());
		InterSession entity = this.getDAO().saveAndFlush(this.getModelMapper().map(ent, InterSession.class));
		return this.getModelMapper().map(entity, InterSessionFullDTO.class);
	}
	
	@Override
	public InterSessionFullDTO update(InterSessionFullDTO ent) {
		ent.setUpdateDate(new Date());
		InterSession entity = this.getDAO().saveAndFlush(this.getModelMapper().map(ent, InterSession.class));
		return this.getModelMapper().map(entity, InterSessionFullDTO.class);
	}
	
	@Override
	public IInterSessionRepository getDAO() {
	    return this.interSessionRepository;
	}
	
	@Override
	public List<InterSessionBasicDTO> findAllInterSession() {
		List<InterSession> interSessions = getDAO().findAll();
	    return interSessions.stream()
	            .map(interSession -> modelMapper.map(interSession, InterSessionBasicDTO.class))
	            .collect(Collectors.toList());
	}
	
}
