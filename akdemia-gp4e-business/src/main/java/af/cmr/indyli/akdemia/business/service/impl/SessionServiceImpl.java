package af.cmr.indyli.akdemia.business.service.impl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import af.cmr.indyli.akdemia.business.dao.ISessionRepository;
import af.cmr.indyli.akdemia.business.dto.basic.SessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.SessionFullDTO;
import af.cmr.indyli.akdemia.business.entity.Session;
import af.cmr.indyli.akdemia.business.service.ISessionService;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;
import jakarta.annotation.Resource;

/**
 * Service implementation class for managing {@link Session} entity, extending the
 * AbstractAkdemiaServiceImpl class. This class provides specific functionality
 * for managing Session, including CRUD operations.
 *
 * @see AbstractAkdemiaServiceImpl
 */
@Service(ConstsValues.ServiceKeys.SESSION_SERVICE_KEY)
public class SessionServiceImpl extends AbstractAkdemiaServiceImpl<Session, SessionBasicDTO, SessionFullDTO, ISessionRepository> implements ISessionService {


	@Resource(name = ConstsValues.ConstsDAO.SESSION_DAO_KEY)
	private ISessionRepository sessionRepository;
	 
	private final ModelMapper modelMapper;
	 
	 
	@Autowired
	public SessionServiceImpl(ISessionRepository sessionRepository, ModelMapper modelMapper) {
	    super(Session.class, SessionBasicDTO.class, SessionFullDTO.class);
	    this.sessionRepository = sessionRepository;
	    this.modelMapper = modelMapper;
	}
 

	@Override
	public SessionFullDTO create(SessionFullDTO ent) {
		ent.setCreationDate(new Date());
		Session entity = this.getDAO().saveAndFlush(this.getModelMapper().map(ent, Session.class));
		return this.getModelMapper().map(entity, SessionFullDTO.class);
	}
	
	@Override
	public ISessionRepository getDAO() {
	    return this.sessionRepository;
	}
	
	@Override
	public List<SessionBasicDTO> findAllSession() {
		List<Session> sessions = getDAO().findAll();
	    return sessions.stream()
	            .map(session -> modelMapper.map(session, SessionBasicDTO.class))
	            .collect(Collectors.toList());
	}

}
