package af.cmr.indyli.akdemia.business.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import af.cmr.indyli.akdemia.business.entity.IntraSession;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;

/**
 * This interface serves as a repository for managing {@link IntraSession} entities
 * in the database. It extends JpaRepository, providing CRUD operations for the
 * Session entity with Integer as the type of its primary key.
 */
@Repository(value = ConstsValues.ConstsDAO.INTRA_DAO_KEY)
public interface IIntraSessionRepository extends JpaRepository<IntraSession, Integer> {
	List<IntraSession> findAll();
}
