package af.cmr.indyli.akdemia.business.service;

import java.util.List;

import af.cmr.indyli.akdemia.business.dao.IIntraSessionRepository;
import af.cmr.indyli.akdemia.business.dto.basic.IntraSessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.IntraSessionFullDTO;
import af.cmr.indyli.akdemia.business.entity.IntraSession;
import af.cmr.indyli.akdemia.business.exception.AkdemiaBusinessException;

/**
 * Interface extending the IAbstractAkdemiaService interface for managing session,
 * providing specific operations for {@link IntraSessio} entity.
 *
 * @see IAbstractAkdemiaService
 */
public interface IIntraSessionService 
extends IAbstractAkdemiaService<IntraSession,IntraSessionBasicDTO, IntraSessionFullDTO, IIntraSessionRepository>{
	List<IntraSessionBasicDTO> findAllIntraSession();
	

	IntraSessionFullDTO create(IntraSessionFullDTO ent) throws AkdemiaBusinessException;
}
