package af.cmr.indyli.akdemia.business.dto.full;

import af.cmr.indyli.akdemia.business.dto.medium.EmployeeSouscriptionMediumDTO;
import af.cmr.indyli.akdemia.business.entity.EmployeeSouscription;

/**
 * This class represents a full Data Transfer Object (DTO) for a
 * {@link EmployeeSouscription} entity, it extends {@link EmployeeSouscriptionMediumDTO}.
 */
public class EmployeeSouscriptionFullDTO extends EmployeeSouscriptionMediumDTO {

}
