package af.cmr.indyli.akdemia.business.dto.medium;

import af.cmr.indyli.akdemia.business.dto.basic.CompanyBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.IntraSessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.TrainerBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.TrainingBasicDTO;

/**
 * This class represents a medium Data Transfer Object (DTO) for a
 * {@link IntraSession} entity. It extends {@link IntraSessionBasicDTO} and inherits
 * basic information about a sub-topic. Medium DTOs typically include additional
 * details beyond the basic DTO but exclude complex associations like lists.
 */
public class IntraSessionMediumDTO extends IntraSessionBasicDTO  {
  private TrainerBasicDTO trainer;
  private TrainingBasicDTO training;
  private CompanyBasicDTO company;
	


	public TrainerBasicDTO getTrainer() {
		return trainer;
	}

	public void setTrainer(TrainerBasicDTO trainer) {
		this.trainer = trainer;
	}

	public TrainingBasicDTO getTraining() {
		return training;
	}

	public void setTraining(TrainingBasicDTO training) {
		this.training = training;
	}
  
  public CompanyBasicDTO getCompany() {
		return company;
	}

	public void setCompany(CompanyBasicDTO company) {
		this.company = company;
	}
}
