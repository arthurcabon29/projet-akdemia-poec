package af.cmr.indyli.akdemia.business.dto.full;

import java.util.ArrayList;
import java.util.List;

import af.cmr.indyli.akdemia.business.dto.basic.EvaluationBasicDTO;
import af.cmr.indyli.akdemia.business.dto.medium.IntraSessionMediumDTO;

public class IntraSessionFullDTO extends IntraSessionMediumDTO {
	private List<EvaluationBasicDTO> evaluations = new ArrayList<>();

//  private List<EmployerSouscription> employerSouscription = new ArrayList<>(); //// ?????

	public List<EvaluationBasicDTO> getEmployees() {
		return evaluations;
	}

	public void setEmployees(List<EvaluationBasicDTO> evaluations) {
		this.evaluations = evaluations;
	}
}
