package af.cmr.indyli.akdemia.business.service.impl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import af.cmr.indyli.akdemia.business.dao.IIntraSessionRepository;
import af.cmr.indyli.akdemia.business.dto.basic.IntraSessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.IntraSessionFullDTO;
import af.cmr.indyli.akdemia.business.entity.IntraSession;
import af.cmr.indyli.akdemia.business.service.IIntraSessionService;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;
import jakarta.annotation.Resource;

/**
 * Service implementation class for managing {@link IntraSession} entity, extending the
 * AbstractAkdemiaServiceImpl class. This class provides specific functionality
 * for managing IntraSession, including CRUD operations.
 *
 * @see AbstractAkdemiaServiceImpl
 */
@Service(ConstsValues.ServiceKeys.INTRA_SERVICE_KEY)
public class IntraSessionServiceImpl extends AbstractAkdemiaServiceImpl<IntraSession, IntraSessionBasicDTO, IntraSessionFullDTO, IIntraSessionRepository> implements IIntraSessionService {

	@Resource(name = ConstsValues.ConstsDAO.INTRA_DAO_KEY)
	private IIntraSessionRepository intraSessionRepository;
	 
	private final ModelMapper modelMapper;
	
	@Autowired
	public IntraSessionServiceImpl(IIntraSessionRepository intraSessionRepository, ModelMapper modelMapper) {
	    super(IntraSession.class, IntraSessionBasicDTO.class, IntraSessionFullDTO.class);
	    this.intraSessionRepository = intraSessionRepository;
	    this.modelMapper = modelMapper;
	}
	
	
	@Override
	public IntraSessionFullDTO create(IntraSessionFullDTO ent) {
		ent.setCreationDate(new Date());
		IntraSession entity = this.getDAO().saveAndFlush(this.getModelMapper().map(ent, IntraSession.class));
		return this.getModelMapper().map(entity, IntraSessionFullDTO.class);
	}
	
	@Override
	public IntraSessionFullDTO update(IntraSessionFullDTO ent) {
		ent.setUpdateDate(new Date());
		IntraSession entity = this.getDAO().saveAndFlush(this.getModelMapper().map(ent, IntraSession.class));
		return this.getModelMapper().map(entity, IntraSessionFullDTO.class);
	}
	
	@Override
	public IIntraSessionRepository getDAO() {
	    return this.intraSessionRepository;
	}
	
	@Override
	public List<IntraSessionBasicDTO> findAllIntraSession() {
		List<IntraSession> intraSessions = getDAO().findAll();
	    return intraSessions.stream()
	            .map(intraSession -> modelMapper.map(intraSession, IntraSessionBasicDTO.class))
	            .collect(Collectors.toList());
	}
	
}
