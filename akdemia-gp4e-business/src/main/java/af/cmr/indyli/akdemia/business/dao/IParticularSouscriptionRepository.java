package af.cmr.indyli.akdemia.business.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import af.cmr.indyli.akdemia.business.entity.Particular;
import af.cmr.indyli.akdemia.business.entity.ParticularSouscription;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;

/**
 * This interface serves as a repository for managing {@link ParticularSouscription} entities
 * in the database. It extends JpaRepository, providing CRUD operations for the
 * ParticularSouscription entity with Integer as the type of its primary key.
 */
@Repository(value = ConstsValues.ConstsDAO.PSOUSCRIPTION_DAO_KEY)
public interface IParticularSouscriptionRepository  extends JpaRepository<ParticularSouscription, Integer> {
	List<ParticularSouscription> findAll();
	
	@Query("SELECT p FROM ParticularSouscription ps JOIN ps.interSession s JOIN ps.particular p WHERE s.id = :idInterSession")
	List<Particular> findAllbyIdInter(@Param("idInterSession") int id);
}
