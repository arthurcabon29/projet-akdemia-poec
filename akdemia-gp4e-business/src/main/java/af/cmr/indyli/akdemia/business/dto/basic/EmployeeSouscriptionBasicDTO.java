package af.cmr.indyli.akdemia.business.dto.basic;

import java.util.Date;

import af.cmr.indyli.akdemia.business.dto.IDTO;

/**
 * This class represents a basic Data Transfer Object (DTO) for a
 * {@link EmployeeSouscription} entity. It encapsulates atomic information about a employeeSouscription.
 */
public class EmployeeSouscriptionBasicDTO implements IDTO {

	private Integer id;
	private String status;
	private Date creationDate;
	private Date updateDate;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	@Override
	public Date getCreationDate() {
		return creationDate;
	}
	@Override
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	@Override
	public Date getUpdateDate() {
		return updateDate;
	}
	@Override
	public void setUpdateDate(Date creationDate) {
		this.creationDate = creationDate; 
	}
}
