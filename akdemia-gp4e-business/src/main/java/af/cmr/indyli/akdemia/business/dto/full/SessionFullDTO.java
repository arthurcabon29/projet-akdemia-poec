package af.cmr.indyli.akdemia.business.dto.full;

import java.util.ArrayList;
import java.util.List;

import af.cmr.indyli.akdemia.business.dto.basic.EvaluationBasicDTO;
import af.cmr.indyli.akdemia.business.dto.medium.SessionMediumDTO;

public class SessionFullDTO extends SessionMediumDTO {
	private List<EvaluationBasicDTO> evaluations = new ArrayList<>();


	public List<EvaluationBasicDTO> getEmployees() {
		return evaluations;
	}

	public void setEmployees(List<EvaluationBasicDTO> evaluations) {
		this.evaluations = evaluations;
	}

}
