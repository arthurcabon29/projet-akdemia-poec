package af.cmr.indyli.akdemia.business.entity;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

/**
 * This class represents an InterSession entity.
 */
@Entity
@Table(name = "AKDEMIA_INTER_SESSION")
public class InterSession extends Session {

	@Column(name = "MIN_PARTICIPANTS")
	private Integer minParticipant;
	
	@OneToMany(mappedBy = "interSession")
	private List<ParticularSouscription> particularSouscriptions;
	
	public Integer getMinParticipant() {
		return minParticipant;
	}

	public void setMinParticipant(Integer minParticipant) {
		this.minParticipant = minParticipant;
	}
	
	public List<ParticularSouscription> getParticularSouscription() {
		return particularSouscriptions;
	}

	public void setParticularSouscription(List<ParticularSouscription> particularSouscriptions) {
		this.particularSouscriptions = particularSouscriptions;
	}
	
	
}
