package af.cmr.indyli.akdemia.business.service.impl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import af.cmr.indyli.akdemia.business.dao.IParticularSouscriptionRepository;
import af.cmr.indyli.akdemia.business.dto.basic.ParticularBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.ParticularSouscriptionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.SessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.ParticularSouscriptionFullDTO;
import af.cmr.indyli.akdemia.business.dto.full.SessionFullDTO;
import af.cmr.indyli.akdemia.business.entity.Particular;
import af.cmr.indyli.akdemia.business.entity.ParticularSouscription;
import af.cmr.indyli.akdemia.business.service.IParticularSouscriptionService;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;
import jakarta.annotation.Resource;

/**
 * Service implementation class for managing {@link ParticularSouscription} entity, extending the
 * AbstractAkdemiaServiceImpl class. This class provides specific functionality
 * for managing ParticularSouscription, including CRUD operations.
 *
 * @see AbstractAkdemiaServiceImpl
 */
@Service(ConstsValues.ServiceKeys.PSOUSCRIPTION_SERVICE_KEY)
public class ParticularSouscriptionServiceImpl extends AbstractAkdemiaServiceImpl<ParticularSouscription, ParticularSouscriptionBasicDTO, ParticularSouscriptionFullDTO, IParticularSouscriptionRepository> implements IParticularSouscriptionService  {


	@Resource(name = ConstsValues.ConstsDAO.PSOUSCRIPTION_DAO_KEY)
	private IParticularSouscriptionRepository particularSouscriptionRepository;
	 
	private final ModelMapper modelMapper;
	 
	 
	@Autowired
	public ParticularSouscriptionServiceImpl(IParticularSouscriptionRepository particularSouscriptionRepository, ModelMapper modelMapper) {
	    super(ParticularSouscription.class, ParticularSouscriptionBasicDTO.class, ParticularSouscriptionFullDTO.class);
	    this.particularSouscriptionRepository = particularSouscriptionRepository;
	    this.modelMapper = modelMapper;
	}
 

	@Override
	public ParticularSouscriptionFullDTO create(ParticularSouscriptionFullDTO ent) {
		ent.setCreationDate(new Date());
		ParticularSouscription entity = this.getDAO().saveAndFlush(this.getModelMapper().map(ent, ParticularSouscription.class));
		return this.getModelMapper().map(entity, ParticularSouscriptionFullDTO.class);
	}
	
	@Override
	public IParticularSouscriptionRepository getDAO() {
	    return this.particularSouscriptionRepository;
	}
	
	@Override
	public List<ParticularSouscriptionBasicDTO> findAllParticularSouscription() {
		List<ParticularSouscription> particularSouscriptions = getDAO().findAll();
	    return particularSouscriptions.stream()
	            .map(particularSouscription -> modelMapper.map(particularSouscription, ParticularSouscriptionBasicDTO.class))
	            .collect(Collectors.toList());
	}
	
	public List<ParticularBasicDTO> findAllParticularSouscriptionByInter(int id) {
		List<Particular> particulars = getDAO().findAllbyIdInter(id);
	    return particulars.stream()
	            .map(particularSouscription -> modelMapper.map(particularSouscription, ParticularBasicDTO.class))
	            .collect(Collectors.toList());
	}

}
