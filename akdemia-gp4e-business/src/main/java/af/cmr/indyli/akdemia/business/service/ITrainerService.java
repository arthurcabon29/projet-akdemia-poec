package af.cmr.indyli.akdemia.business.service;

import java.util.List;

import af.cmr.indyli.akdemia.business.dao.ITrainerRepository;
import af.cmr.indyli.akdemia.business.dto.basic.TrainerBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.TrainerFullDTO;
import af.cmr.indyli.akdemia.business.entity.Trainer;
import af.cmr.indyli.akdemia.business.exception.AkdemiaBusinessException;

/**
 * Interface extending the IAbstractAkdemiaService interface for managing
 * employees, providing specific operations for {@link Trainer} entities.
 *
 * @see IAbstractAkdemiaService
 */
public interface ITrainerService extends IAbstractAkdemiaService<Trainer, TrainerBasicDTO, TrainerFullDTO, ITrainerRepository> {

//	public TrainerFullDTO findEmployeeByCompany(Integer idCompany) throws AkdemiaBusinessException;

	public List<TrainerFullDTO> findAllFull();
}

