package af.cmr.indyli.akdemia.business.dto.medium;

import af.cmr.indyli.akdemia.business.dto.basic.TrainerBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.ValidationTestBasicDTO;

public class TrainerMediumDTO extends TrainerBasicDTO {
	private ValidationTestBasicDTO validationTest;

	public ValidationTestBasicDTO getValidationTest() {
		return validationTest;
	}

	public void setValidationTest(ValidationTestBasicDTO validationTest) {
		this.validationTest = validationTest;
	}
}
