package af.cmr.indyli.akdemia.business.dto.full;

import java.util.ArrayList;
import java.util.List;

import af.cmr.indyli.akdemia.business.dto.basic.EmployeeSouscriptionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.EvaluationBasicDTO;
import af.cmr.indyli.akdemia.business.dto.medium.EmployeeMediumDTO;
import af.cmr.indyli.akdemia.business.entity.Employee;

/**
 * This class represents a full Data Transfer Object (DTO) for a
 * {@link Employee} entity, it extends {@link EmployeeMediumDTO}.
 */
public class EmployeeFullDTO extends EmployeeMediumDTO {
	

	private List<EmployeeSouscriptionBasicDTO> employeeSouscription = new ArrayList<>();

	public EmployeeFullDTO() {
	}

	public List<EmployeeSouscriptionBasicDTO> getEmployeeSouscription() {
		return employeeSouscription;
	}

	public void setEmployeeSouscription(List<EmployeeSouscriptionBasicDTO> employeeSouscription) {
		this.employeeSouscription = employeeSouscription;
	}
}
