package af.cmr.indyli.akdemia.business.service;

import java.util.List;

import af.cmr.indyli.akdemia.business.dao.IInterSessionRepository;
import af.cmr.indyli.akdemia.business.dto.basic.InterSessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.InterSessionFullDTO;
import af.cmr.indyli.akdemia.business.entity.InterSession;
import af.cmr.indyli.akdemia.business.exception.AkdemiaBusinessException;

/**
 * Interface extending the IAbstractAkdemiaService interface for managing session,
 * providing specific operations for {@link InterSessio} entity.
 *
 * @see IAbstractAkdemiaService
 */
public interface IInterSessionService 
extends IAbstractAkdemiaService<InterSession, InterSessionBasicDTO, InterSessionFullDTO, IInterSessionRepository>{
	List<InterSessionBasicDTO> findAllInterSession();
	

	InterSessionFullDTO create(InterSessionFullDTO ent) throws AkdemiaBusinessException;
}
