package af.cmr.indyli.akdemia.business.entity;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

/**
 * This class represents an Trainer entity. It encapsulates information about
 * an individual who animates training.
 */
@Entity
@Table(name = "AKDEMIA_TRAINER")
public class Trainer extends User {

	@Column(name = "ACTIVITY")
	private String activity;
	
	@Column(name = "FIRSTNAME")
	private String firstname;

	@Column(name = "LASTNAME")
	private String lastname;

	@Column(name = "GENDER")
	private String gender;

	@Column(name = "CV_LINK")
	private String cvLink;
	
	@ManyToOne
	@JoinColumn(name = "ID_AKDEMIA_VALIDATION_TEST")
	private ValidationTest validationTest;
	
	@OneToMany(mappedBy = "trainer")
	private List<Session> sessions; 
	
	

	public Trainer () {
	}
	
	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCvLink() {
		return cvLink;
	}

	public void setCvLink(String cvLink) {
		this.cvLink = cvLink;
	}

	public ValidationTest getValidationTest() {
		return validationTest;
	}

	public void setValidationTest(ValidationTest validationTest) {
		this.validationTest = validationTest;
	}
	


}
