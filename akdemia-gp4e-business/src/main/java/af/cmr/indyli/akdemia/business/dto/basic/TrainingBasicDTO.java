package af.cmr.indyli.akdemia.business.dto.basic;

import java.util.Date;

import af.cmr.indyli.akdemia.business.dto.IDTO;


public class TrainingBasicDTO implements IDTO {
	
	   private Integer id;
		
	  
	   private String trainingTitle;
	   
	   
	   private String trainingDescription;
	   
	   
	   private double trainingPrice; 
	   
	  
	   private Date creationDate;
	   
	   
	   private Date updateDate;
	   
	   private String logo;
	   
	   public TrainingBasicDTO() {
		}

	   
	public TrainingBasicDTO(String trainingTitle) {
		this.trainingTitle = trainingTitle; 
		}
	
	public TrainingBasicDTO(String trainingTitle,String trainingDescription,double trainingPrice, Date creationDate  ) {
		this.trainingTitle = trainingTitle; 
		this.trainingDescription = trainingDescription; 
		this.trainingPrice = trainingPrice; 
		this.creationDate = creationDate; 
		}
	

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
		
	}

	@Override
	public Date getCreationDate() {
		
		return creationDate;
	}

	@Override
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate; 
		
	}

	@Override
	public Date getUpdateDate() {
		
		return updateDate;
	}

	@Override
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
		
	}
	
	public String getLogo() {
		return logo;
}


public void setLogo(String logo) {
	this.logo = logo;
	
}
	
	public String getTrainingTitle() {
		return trainingTitle;
}

	

public void setTrainingTitle(String trainingTitle) {
	this.trainingTitle = trainingTitle;
	
}

public String getTrainingDescription() {
	return trainingDescription ;
}

public void setTrainingDescription(String trainingDescription) {
	this.trainingDescription = trainingDescription;
	
}

public double getTrainingPrice() {
	return trainingPrice;
}

public void setTrainingPrice(double trainingPrice) {
	this.trainingPrice = trainingPrice;
	
}



}
