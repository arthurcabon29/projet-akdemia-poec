package af.cmr.indyli.akdemia.business.entity;

import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

/**
 * This class represents an Employee entity. It encapsulates information about
 * an individual intern within the company.
 */
@Entity
@Table(name = "AKDEMIA_INTRA_SESSION")
public class IntraSession extends Session {


	@OneToMany(mappedBy = "intraSession")
	private List<EmployeeSouscription> employerSouscriptions;
	
	@ManyToOne
    @JoinColumn(name = "ID_AKDEMIA_COMPANY")
    private Company company;
	
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public List<EmployeeSouscription> getEmployerSouscriptions() {
		return employerSouscriptions;
	}

	public void setEmployerSouscriptions(List<EmployeeSouscription> employerSouscriptions) {
		this.employerSouscriptions = employerSouscriptions;
	}
}
