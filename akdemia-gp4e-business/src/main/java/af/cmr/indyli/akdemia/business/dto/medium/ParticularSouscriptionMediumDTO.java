package af.cmr.indyli.akdemia.business.dto.medium;

import af.cmr.indyli.akdemia.business.dto.basic.InterSessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.ParticularBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.ParticularSouscriptionBasicDTO;

/**
 * This class represents a medium Data Transfer Object (DTO) for a
 * {@link ParticularSouscription} entity. It extends {@link ParticularSouscriptionMediumDTO}.
 * Medium DTOs typically include additional
 * details beyond the basic DTO but exclude complex associations like lists.
 */
public class ParticularSouscriptionMediumDTO extends ParticularSouscriptionBasicDTO{
	private InterSessionBasicDTO interSession;
	private ParticularBasicDTO particular;
	
	public InterSessionBasicDTO getInterSession() {
		return interSession;
	}
	public void setInterSession(InterSessionBasicDTO interSession) {
		this.interSession = interSession;
	}
	public ParticularBasicDTO getParticular() {
		return particular;
	}
	public void setParticular(ParticularBasicDTO particular) {
		this.particular = particular;
	}
}
