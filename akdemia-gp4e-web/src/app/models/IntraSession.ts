export interface IntraSession {
  id: number;
  code: string;
  duration: string;
  price: number;
  description: string;
  status: string;
  date: Date;
  lacation: String;
  sessionScore: number;
  creationDate: Date;
  updateDate: Date;
  // trainer: Trainer;
  // training: Training;
}
