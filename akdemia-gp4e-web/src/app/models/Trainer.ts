import { User } from './User';

export interface Trainer extends User {
  firstname: string;
  lastname: string;
  activity: string;
  cvLink: string;
  gender: string;
}
