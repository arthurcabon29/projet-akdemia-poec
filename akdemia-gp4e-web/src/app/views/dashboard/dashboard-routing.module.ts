import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { ContentDashboardComponent } from '../content-dashboard/content-dashboard.component';
import { ThemesComponent } from '../themes/themes.component';
import { ClientsComponent } from '../clients/clients.component';
import { ThemesInfosComponent } from '../themes/themes-infos/themes-infos.component';
import { InsertClientComponent } from '../clients/insert-client/insert-client.component';
import { RouterModule, Routes } from '@angular/router';
import { SessionComponent } from '../session/session.component';
import { InsertSessionComponent } from '../session/insert-session/insert-session/insert-session.component';
import { TrainersComponent } from '../trainers/trainers/trainers.component';
import { InsertTrainerComponent } from '../trainers/insert-trainer/insert-trainer.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: '', component: ContentDashboardComponent },
      { path: 'catalogues/themes', component: ThemesComponent },
      { path: 'clients', component: ClientsComponent },
      { path: 'catalogues/themes/infos/:id', component: ThemesInfosComponent },
      { path: 'clients/insert', component: InsertClientComponent },
      { path: 'clients/:id', component: InsertClientComponent },
      { path: 'clients/employe/:id', component: InsertClientComponent },
      { path: 'clients/particulier/:id', component: InsertClientComponent },
      { path: 'clients/company/:id', component: InsertClientComponent },
      { path: 'sessions', component: SessionComponent },
      { path: 'sessions/insert', component: InsertSessionComponent },
      { path: 'sessions/interSession/:id', component: InsertSessionComponent },
      { path: 'sessions/intraSession/:id', component: InsertSessionComponent },
      { path: 'formateurs', component: TrainersComponent },
      { path: 'formateurs/insert', component: InsertTrainerComponent },
      { path: 'formateurs/:id', component: InsertTrainerComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
