import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { InterSession } from 'src/app/models/InterSession';
import { IntraSession } from 'src/app/models/IntraSession';
import { AlertService } from 'src/app/services/alert.service';
import { InterSessionService } from 'src/app/services/inter-session.service';
import { IntraSessionService } from 'src/app/services/intra-session.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss'],
})
export class SessionComponent implements OnInit {
  interSessions: InterSession[] = [];
  intraSessions: IntraSession[] = [];
  isLoading!: boolean;

  showInter: boolean = true;
  showIntra: boolean = false;

  //for search
  interSessionReserved: InterSession[] = [];
  intraSessionReserved: IntraSession[] = [];

  interSessionSearch: InterSession[] = [];
  intraSessionsSearch: IntraSession[] = [];

  //for filter
  filterForm!: FormGroup;
  searchForm!: FormGroup;
  //for pagination
  page: number = 1;

  constructor(
    private router: Router,
    private interSessionService: InterSessionService,
    private intraSessionService: IntraSessionService,
    private alert: AlertService
  ) {}

  ngOnInit(): void {
    this.getAllInterSessions();
    this.getAllIntraSessions();
    this.initForm();
  }

  initForm() {
    this.searchForm = new FormGroup({
      keyWord: new FormControl(''),
    });

    this.filterForm = new FormGroup({
      filter: new FormControl(20),
    });
  }

  handlePageChange(event: number) {
    this.page = event;
  }

  searchByTitle() {
    //for interSession
    if (this.showInter) {
      this.interSessions = this.interSessionReserved;
      let table: InterSession[] = [];
      for (let i = 0; i < this.interSessions.length; i++) {
        if (
          this.interSessions[i].code
            .toLowerCase()
            .includes(this.searchForm.value.keyWord.toLowerCase())
        ) {
          table.push(this.interSessions[i]);
        }
      }
      if (this.searchForm.value.keyWord.trim() == '') {
        this.interSessions = this.interSessionReserved;
      } else {
        this.interSessions = table;
      }
    }
    //for intraSession
    else if (this.showIntra) {
      this.intraSessions = this.intraSessionReserved;
      let table: IntraSession[] = [];
      for (let i = 0; i < this.intraSessions.length; i++) {
        if (
          this.intraSessions[i].code
            .toLowerCase()
            .includes(this.searchForm.value.keyWord.toLowerCase())
        ) {
          table.push(this.intraSessions[i]);
        }
      }
      if (this.searchForm.value.keyWord.trim() == '') {
        this.intraSessions = this.intraSessionReserved;
      } else {
        this.intraSessions = table;
      }
    }
  }

  getAllInterSessions() {
    this.interSessionService.getAll().subscribe(
      (data) => {
        this.interSessions = data;
        this.interSessionReserved = data;
      },
      (err) => {
        this.alert.alertError(
          err.error !== null
            ? err.error.message
            : "Une erreur s'est produite lors de la récupération des sessions inter"
        );
      }
    );
  }

  getAllIntraSessions() {
    this.intraSessionService.getAll().subscribe(
      (data) => {
        this.intraSessions = data;
        this.intraSessionReserved = data;
      },
      (err) => {
        this.alert.alertError(
          err.error !== null
            ? err.error.message
            : "Une erreur s'est produite lors de la récupération des sessions intra"
        );
      }
    );
  }

  goToEditInterSess(id: number) {
    this.router.navigateByUrl(`dashboard/sessions/interSession/${id}`);
  }

  goToEditIntraSess(id: number) {
    this.router.navigateByUrl(`dashboard/sessions/intraSession/${id}`);
  }

  deleteSession(id: number, sessionType: string) {
    Swal.fire({
      title: 'Etes-vous sûr de vouloir effectuer cette suppression?',
      text: 'Cette action est irréversible!',
      icon: 'warning',
      cancelButtonText: 'Annuler',
      showCancelButton: true,
      confirmButtonColor: '#0d6efd',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, Supprimer!',
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        if (sessionType == 'interSession') {
          this.interSessionService.delete(id).subscribe(
            () => {
              this.getAllInterSessions();
              Swal.fire(
                'Supprimé!',
                'La session inter a été supprimé avec succès.',
                'success'
              );
            },
            (err) => {
              this.alert.alertError(
                err.error !== null
                  ? err.error.message
                  : 'Impossible de supprimer la session inter'
              );
            }
          );
        } else if (sessionType == 'intraSesson') {
          this.intraSessionService.delete(id).subscribe(
            () => {
              this.getAllIntraSessions();
              Swal.fire(
                'supprimé!',
                'La session intra a été supprimé avec succès.',
                'success'
              );
            },
            (err) => {
              this.alert.alertError(
                err.error !== null
                  ? err.error.message
                  : 'Impossible de supprimer la session intra'
              );
            }
          );
        }
      }
    });
  }

  showInterSession() {
    this.showInter = true;
    this.showIntra = false;
  }

  showIntraSession() {
    this.showInter = false;
    this.showIntra = true;
  }
}
