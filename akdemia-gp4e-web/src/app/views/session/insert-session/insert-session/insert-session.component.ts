import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { InterSessionService } from 'src/app/services/inter-session.service';
import { IntraSessionService } from 'src/app/services/intra-session.service';
import { SessionFormService } from 'src/app/views/forms/session-form.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-insert-session',
  templateUrl: './insert-session.component.html',
  styleUrls: ['./insert-session.component.scss'],
})
export class InsertSessionComponent implements OnInit {
  constructor(
    private interSessionService: InterSessionService,
    private intraSessionService: IntraSessionService,
    private router: Router,
    private route: ActivatedRoute,
    private alert: AlertService
  ) {}

  // companies: Company[] = [];

  // entreprise: Partial<Company> = {};
  interSession!: string;
  intraSession!: string;
  sessionForm!: FormGroup;
  formVisibility!: string;
  isLoading!: boolean;
  idSession!: any;
  isFormEdit!: boolean;
  curentUri!: string;
  title: string = 'Enregistrer une session';

  // employeeValue!: Employee;

  ngOnInit(): void {
    this.idSession = this.route.snapshot.paramMap.get('id');
    if (this.idSession) {
      this.title = 'Modifier les informations';
      this.formVisibility = this.route.snapshot.url[1]?.path || '';
      this.getById(this.idSession);

      this.isFormEdit = true;
    }
    this.initForm();
    // this.getAllCompany();
  }

  cancel() {
    this.router.navigate(['dashboard/sessions']);
  }

  getById(id: any) {
    switch (this.formVisibility) {
      case 'interSession':
        this.interSessionService.getById(id).subscribe({
          next: (data) => {
            this.sessionForm.patchValue(data);
            // this.interSession = data.company;
          },
          error: (err) => {
            this.alert.alertError(
              err.error !== null
                ? err.error.message
                : "Impossible de récupérer l'identifiant de la session inter"
            );
          },
        });
        break;
      case 'intraSession':
        this.intraSessionService.getById(id).subscribe({
          next: (data) => {
            this.sessionForm.patchValue(data);
          },
          error: (err) => {
            this.alert.alertError(
              err.error !== null
                ? err.error.message
                : "Impossible de récupérer l'identifiant de la session intra"
            );
          },
        });
        break;
      default:
        break;
    }
  }

  initForm() {
    // Sélectionnez la première entreprise du tableau 'interSession' comme entreprise par défaut
    // new SessionFormService.getSessionForm();
    // new SessionFormService(this.sessionForm)
    this.sessionForm = new FormGroup({
      id: new FormControl(),
      code: new FormControl('', Validators.required),
      duration: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      status: new FormControl(null),
      date: new FormControl('', Validators.required),
      price: new FormControl('', Validators.required),
      lacation: new FormControl('', Validators.required),
      sessionScore: new FormControl('', Validators.required),
      minParticipant: new FormControl('', Validators.required),
      company: new FormControl(),
      trainer: new FormControl(),
      training: new FormControl(),
    });
  }

  updateForm(event: any) {
    this.formVisibility = event.target.value;
  }

  createParticipant() {
    this.isLoading = true;
    let form = this.sessionForm.value;
    if (this.idSession) {
      if (this.formVisibility == 'interSession') {
        console.log(form);

        this.interSessionService.edit(this.idSession, form).subscribe(
          (data) => {
            this.isLoading = false;
            Swal.fire(
              'Modifié!',
              "L'interSession a été modifié avec succès.",
              'success'
            );
            this.router.navigate(['dashboard/sessions']);
          },
          (err) => {
            this.alert.alertError(
              err.error !== null
                ? err.error.message
                : "Une erreur s'est produite lors de la modification de l'employé"
            );
          }
        );
      } else if (this.formVisibility == 'intraSession') {
        console.log(form);
        this.intraSessionService.edit(this.idSession, form).subscribe(
          (data) => {
            this.isLoading = false;
            Swal.fire(
              'Modifié!',
              "L'intraSession a été modifiée avec succès.",
              'success'
            );
            this.router.navigate(['dashboard/sessions']);
          },
          (err) => {
            this.alert.alertError(
              err.error !== null
                ? err.error.message
                : "Une erreur s'est produite lors de la modification de l'entreprise"
            );
          }
        );
      }
    } else {
      if (this.formVisibility == 'interSession') {
        console.log(form);

        this.interSessionService.save(form).subscribe(
          (data) => {
            this.isLoading = false;
            Swal.fire(
              'Ajouté!',
              "L'employé a été ajouté avec succès.",
              'success'
            );

            this.router.navigate(['dashboard/sessions']);
          },
          (err) => {
            this.alert.alertError(
              err.error !== null
                ? err.error.message
                : "Une erreur s'est produite lors de l'ajout de l'employé"
            );
          }
        );
      } else if (this.formVisibility == 'intraSession') {
        this.intraSessionService.save(form).subscribe(
          (data) => {
            this.isLoading = false;
            Swal.fire(
              'Ajouté!',
              'Le particulier a été ajouté avec succès.',
              'success'
            );
            this.router.navigate(['dashboard/sessions']);
          },
          (err) => {
            this.alert.alertError(
              err.error !== null
                ? err.error.message
                : "Une erreur s'est produite lors de l'ajout du pariculier"
            );
          }
        );
      }
    }
  }
}
