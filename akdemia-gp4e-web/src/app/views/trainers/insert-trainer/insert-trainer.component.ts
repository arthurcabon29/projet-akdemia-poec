import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Trainer } from 'src/app/models/Trainer';
import { AlertService } from 'src/app/services/alert.service';
import { TrainerService } from 'src/app/services/trainer.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-insert-trainer',
  templateUrl: './insert-trainer.component.html',
  styleUrls: ['./insert-trainer.component.scss'],
})
export class InsertTrainerComponent implements OnInit {
  constructor(
    private trainerService: TrainerService,
    private router: Router,
    private route: ActivatedRoute,
    private alert: AlertService
  ) {}

  trainers: Trainer[] = [];

  userForm!: FormGroup;
  isLoading!: boolean;
  idTrainer!: any;
  isFormEdit!: boolean;
  curentUri!: string;
  title: string = 'Enregistrer un formateur';

  trainerValue!: Trainer;

  ngOnInit(): void {
    this.idTrainer = this.route.snapshot.paramMap.get('id');
    if (this.idTrainer) {
      this.title = 'Modifier les informations';
      this.getById(this.idTrainer);

      this.isFormEdit = true;
    }
    this.initForm();
    this.getAllTrainers();
  }

  cancel() {
    this.router.navigate(['dashboard/formateurs']);
  }

  getById(id: any) {
    this.trainerService.getById(id).subscribe({
      next: (data) => {
        this.userForm.patchValue(data);
      },
      error: (err) => {
        this.alert.alertError(
          err.error !== null
            ? err.error.message
            : "Impossible de récupérer l'identifiant du formateur"
        );
      },
    });
  }

  getAllTrainers() {
    this.trainerService.getAll().subscribe(
      (data) => {
        this.trainers = data;
      },
      (err) => {
        this.alert.alertError(
          err.error !== null
            ? err.error.message
            : 'Impossible de récupérer les formateurs'
        );
      }
    );
  }

  initForm() {
    // Sélectionnez le premier formateur du tableau 'trainers' comme formateur par défaut
    this.userForm = new FormGroup({
      id: new FormControl(),
      firstname: new FormControl(''),
      lastname: new FormControl(''),
      email: new FormControl('', [Validators.required, Validators.email]),
      phone: new FormControl(''),
      address: new FormControl(''),
      activity: new FormControl(''),
      gender: new FormControl('M'),
      cvLink: new FormControl(''),
      creationDate: new FormControl(),
      updateDate: new FormControl(),
    });
  }

  createParticipant() {
    this.isLoading = true;
    let form = this.userForm.value;
    if (this.idTrainer) {
      console.log(form);
      this.trainerService.edit(this.idTrainer, form).subscribe(
        (data) => {
          this.isLoading = false;
          Swal.fire(
            'Modifié!',
            'Le formateur a été modifié avec succès.',
            'success'
          );
          this.router.navigate(['dashboard/formateurs']);
        },
        (err) => {
          this.alert.alertError(
            err.error !== null
              ? err.error.message
              : "Une erreur s'est produite lors de la modification de l'employé"
          );
        }
      );
    } else {
      console.log(form);
      this.trainerService.save(form).subscribe(
        (data) => {
          this.isLoading = false;
          Swal.fire(
            'Ajouté!',
            "L'employé a été ajouté avec succès.",
            'success'
          );

          this.router.navigate(['dashboard/formateurs']);
        },
        (err) => {
          this.alert.alertError(
            err.error !== null
              ? err.error.message
              : "Une erreur s'est produite lors de l'ajout du formateur"
          );
        }
      );
    }
  }
}
