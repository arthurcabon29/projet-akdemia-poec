import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Trainer } from 'src/app/models/Trainer';
import { AlertService } from 'src/app/services/alert.service';
import { TrainerService } from 'src/app/services/trainer.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-trainers',
  templateUrl: './trainers.component.html',
  styleUrls: ['./trainers.component.scss'],
})
export class TrainersComponent implements OnInit {
  trainers: Trainer[] = [];
  isLoading!: boolean;

  showPart: boolean = true;

  //for search
  trainerReserved: Trainer[] = [];

  trainerSearch: Trainer[] = [];

  //for filter
  filterForm!: FormGroup;
  searchForm!: FormGroup;
  //for pagination
  page: number = 1;

  constructor(
    private router: Router,
    private trainerService: TrainerService,
    private alert: AlertService
  ) {}

  ngOnInit(): void {
    this.getAllTrainers();
    this.initForm();
  }

  initForm() {
    this.searchForm = new FormGroup({
      keyWord: new FormControl(''),
    });

    this.filterForm = new FormGroup({
      filter: new FormControl(20),
    });
  }

  handlePageChange(event: number) {
    this.page = event;
  }

  searchByName() {
    if (this.showPart) {
      this.trainers = this.trainerReserved;
      let table: Trainer[] = [];
      for (let i = 0; i < this.trainers.length; i++) {
        if (
          this.trainers[i].email
            .toLowerCase()
            .includes(this.searchForm.value.keyWord.toLowerCase()) ||
          this.trainers[i].firstname
            .toLowerCase()
            .includes(this.searchForm.value.keyWord.toLowerCase())
        ) {
          table.push(this.trainers[i]);
        }
      }
      if (this.searchForm.value.keyWord.trim() == '') {
        this.trainers = this.trainerReserved;
      } else {
        this.trainers = table;
      }
    }
  }

  getAllTrainers() {
    this.isLoading = true;
    this.trainerService.getAll().subscribe(
      (data) => {
        this.trainers = data;
        console.log(data);
        this.trainerReserved = data;
        this.isLoading = false;
      },
      (err) => {
        this.alert.alertError(
          err.error !== null
            ? err.error.message
            : "Une erreur s'est produite lors de la récupération des formateurs"
        );
      }
    );
  }

  goToEditPart(id: number) {
    this.router.navigateByUrl(`dashboard/formateurs/${id}`);
  }

  goToEditClt(id: number) {
    this.router.navigateByUrl(`dashboard/formateurs/${id}`);
  }

  goToEditCpy(id: number) {
    this.router.navigateByUrl(`dashboard/formateurs/${id}`);
  }

  deleteTrainer(id: number) {
    Swal.fire({
      title: 'Etes-vous sûr de vouloir effectuer cette suppression?',
      text: 'Cette action est irréversible!',
      icon: 'warning',
      cancelButtonText: 'Annuler',
      showCancelButton: true,
      confirmButtonColor: '#0d6efd',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, Supprimer!',
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        this.trainerService.delete(id).subscribe(
          () => {
            this.getAllTrainers();
            Swal.fire(
              'Supprimé!',
              'Le formateur a été supprimé avec succès.',
              'success'
            );
          },
          (err) => {
            this.alert.alertError(
              err.error !== null
                ? err.error.message
                : 'Impossible de supprimer un formateur'
            );
          }
        );
      }
    });
  }
}
