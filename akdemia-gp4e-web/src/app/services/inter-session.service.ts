import { Injectable } from '@angular/core';
import { URL_BASE } from '../conf/constant';
import { InterSession } from '../models/InterSession';
import { CrudService } from './crud.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class InterSessionService extends CrudService<InterSession> {
  constructor(http: HttpClient) {
    const url: string = URL_BASE;
    super(http, `${url}/intersessions`);
  }
}
