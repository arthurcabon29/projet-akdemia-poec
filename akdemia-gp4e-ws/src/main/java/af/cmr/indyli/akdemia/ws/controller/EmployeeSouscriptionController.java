package af.cmr.indyli.akdemia.ws.controller;

import java.nio.file.AccessDeniedException;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import af.cmr.indyli.akdemia.business.dto.basic.IntraSessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.EmployeeBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.EmployeeSouscriptionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.IntraSessionFullDTO;
import af.cmr.indyli.akdemia.business.dto.full.EmployeeFullDTO;
import af.cmr.indyli.akdemia.business.dto.full.EmployeeSouscriptionFullDTO;
import af.cmr.indyli.akdemia.business.dto.full.InterSessionFullDTO;
import af.cmr.indyli.akdemia.business.exception.AkdemiaBusinessException;
import af.cmr.indyli.akdemia.business.service.IIntraSessionService;
import af.cmr.indyli.akdemia.business.service.IEmployeeSouscriptionService;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;
import af.cmr.indyli.akdemia.ws.utils.AkdemiaUrlBase;
import jakarta.annotation.Resource;

@RestController
@CrossOrigin(origins = AkdemiaUrlBase.url, maxAge = AkdemiaUrlBase.maxAge)
@RequestMapping("/employeesouscription")
public class EmployeeSouscriptionController {
	@Resource(name = ConstsValues.ServiceKeys.ESOUSCRIPTION_SERVICE_KEY)
	private IEmployeeSouscriptionService employeeSouscriptionService;
	
	@GetMapping
	public ResponseEntity<List<EmployeeSouscriptionBasicDTO>> getAll() {
	return ResponseEntity.ok(employeeSouscriptionService.findAll());
	}
    
	@GetMapping("/{id}")
	public ResponseEntity<EmployeeSouscriptionFullDTO> getOne(@PathVariable int id) throws AkdemiaBusinessException {
	return ResponseEntity.ok(employeeSouscriptionService.findById(id));
	}
	
	@GetMapping("/session/{id}")
	public ResponseEntity<List<EmployeeBasicDTO>> getOneByIdIntra(@PathVariable int id) throws AkdemiaBusinessException {
	return ResponseEntity.ok(employeeSouscriptionService.findAllEmployeeSouscriptionByIntra(id));
	}
	
	@PostMapping
	public ResponseEntity<EmployeeSouscriptionFullDTO> create(@RequestBody EmployeeSouscriptionFullDTO dto) throws AkdemiaBusinessException {
		return ResponseEntity.ok(employeeSouscriptionService.create(dto));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable int id) throws AkdemiaBusinessException, AccessDeniedException {
		employeeSouscriptionService.deleteById(id);
		return ResponseEntity.ok().build();
	}
}
