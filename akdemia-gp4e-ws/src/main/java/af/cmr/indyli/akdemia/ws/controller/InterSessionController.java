package af.cmr.indyli.akdemia.ws.controller;

import java.nio.file.AccessDeniedException;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import af.cmr.indyli.akdemia.business.dto.basic.InterSessionBasicDTO;

import af.cmr.indyli.akdemia.business.dto.full.InterSessionFullDTO;

import af.cmr.indyli.akdemia.business.exception.AkdemiaBusinessException;
import af.cmr.indyli.akdemia.business.service.IInterSessionService;

import af.cmr.indyli.akdemia.business.utils.ConstsValues;
import af.cmr.indyli.akdemia.ws.utils.AkdemiaUrlBase;
import jakarta.annotation.Resource;

@RestController
@CrossOrigin(origins = AkdemiaUrlBase.url, maxAge = AkdemiaUrlBase.maxAge)
@RequestMapping("/intersessions")
public class InterSessionController {
	@Resource(name = ConstsValues.ServiceKeys.INTER_SERVICE_KEY)
	private IInterSessionService interSessionService;
	
	@GetMapping
	public ResponseEntity<List<InterSessionBasicDTO>> getAll() {
	return ResponseEntity.ok(interSessionService.findAll());
	}
    
	@GetMapping("/{id}")
	public ResponseEntity<InterSessionFullDTO> getOne(@PathVariable int id) throws AkdemiaBusinessException {
	return ResponseEntity.ok(interSessionService.findById(id));
	}
	
	@PostMapping
	public ResponseEntity<InterSessionFullDTO> create(@RequestBody InterSessionFullDTO dto) throws AkdemiaBusinessException {
		return ResponseEntity.ok(interSessionService.create(dto));
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<InterSessionFullDTO> update(@PathVariable int id, @RequestBody InterSessionFullDTO dto)
			throws AkdemiaBusinessException, AccessDeniedException {
		return ResponseEntity.ok(interSessionService.update(dto));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable int id) throws AkdemiaBusinessException, AccessDeniedException {
		interSessionService.deleteById(id);
		return ResponseEntity.ok().build();
	}
    
	
	

	
	
}
