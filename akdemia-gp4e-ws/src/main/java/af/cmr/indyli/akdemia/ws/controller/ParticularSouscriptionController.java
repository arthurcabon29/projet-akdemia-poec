package af.cmr.indyli.akdemia.ws.controller;

import java.nio.file.AccessDeniedException;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import af.cmr.indyli.akdemia.business.dto.basic.InterSessionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.ParticularBasicDTO;
import af.cmr.indyli.akdemia.business.dto.basic.ParticularSouscriptionBasicDTO;
import af.cmr.indyli.akdemia.business.dto.full.EmployeeSouscriptionFullDTO;
import af.cmr.indyli.akdemia.business.dto.full.InterSessionFullDTO;
import af.cmr.indyli.akdemia.business.dto.full.ParticularFullDTO;
import af.cmr.indyli.akdemia.business.dto.full.ParticularSouscriptionFullDTO;
import af.cmr.indyli.akdemia.business.exception.AkdemiaBusinessException;
import af.cmr.indyli.akdemia.business.service.IInterSessionService;
import af.cmr.indyli.akdemia.business.service.IParticularSouscriptionService;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;
import af.cmr.indyli.akdemia.ws.utils.AkdemiaUrlBase;
import jakarta.annotation.Resource;

@RestController
@CrossOrigin(origins = AkdemiaUrlBase.url, maxAge = AkdemiaUrlBase.maxAge)
@RequestMapping("/particularsouscription")
public class ParticularSouscriptionController {
	@Resource(name = ConstsValues.ServiceKeys.PSOUSCRIPTION_SERVICE_KEY)
	private IParticularSouscriptionService particularSouscriptionService;
	
	@GetMapping
	public ResponseEntity<List<ParticularSouscriptionBasicDTO>> getAll() {
	return ResponseEntity.ok(particularSouscriptionService.findAll());
	}
    
	@GetMapping("/{id}")
	public ResponseEntity<ParticularSouscriptionFullDTO> getOne(@PathVariable int id) throws AkdemiaBusinessException {
	return ResponseEntity.ok(particularSouscriptionService.findById(id));
	}
	
	@GetMapping("/session/{id}")
	public ResponseEntity<List<ParticularBasicDTO>> getOneByIdInter(@PathVariable int id) throws AkdemiaBusinessException {
	return ResponseEntity.ok(particularSouscriptionService.findAllParticularSouscriptionByInter(id));
	}
	
	@PostMapping
	public ResponseEntity<ParticularSouscriptionFullDTO> create(@RequestBody ParticularSouscriptionFullDTO dto) throws AkdemiaBusinessException {
		return ResponseEntity.ok(particularSouscriptionService.create(dto));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable int id) throws AkdemiaBusinessException, AccessDeniedException {
		particularSouscriptionService.deleteById(id);
		return ResponseEntity.ok().build();
	}
}
