package af.cmr.indyli.akdemia.ws.controller;

import java.nio.file.AccessDeniedException;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import af.cmr.indyli.akdemia.business.dto.basic.IntraSessionBasicDTO;

import af.cmr.indyli.akdemia.business.dto.full.IntraSessionFullDTO;

import af.cmr.indyli.akdemia.business.exception.AkdemiaBusinessException;
import af.cmr.indyli.akdemia.business.service.IIntraSessionService;

import af.cmr.indyli.akdemia.business.utils.ConstsValues;
import af.cmr.indyli.akdemia.ws.utils.AkdemiaUrlBase;
import jakarta.annotation.Resource;

@RestController
@CrossOrigin(origins = AkdemiaUrlBase.url, maxAge = AkdemiaUrlBase.maxAge)
@RequestMapping("/intrasessions")
public class IntraSessionController {
	@Resource(name = ConstsValues.ServiceKeys.INTRA_SERVICE_KEY)
	private IIntraSessionService intraSessionService;
	
	@GetMapping
	public ResponseEntity<List<IntraSessionBasicDTO>> getAll() {
	return ResponseEntity.ok(intraSessionService.findAll());
	}
    
	@GetMapping("/{id}")
	public ResponseEntity<IntraSessionFullDTO> getOne(@PathVariable int id) throws AkdemiaBusinessException {
	return ResponseEntity.ok(intraSessionService.findById(id));
	}
	
	@PostMapping
	public ResponseEntity<IntraSessionFullDTO> create(@RequestBody IntraSessionFullDTO dto) throws AkdemiaBusinessException {
		return ResponseEntity.ok(intraSessionService.create(dto));
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<IntraSessionFullDTO> update(@PathVariable int id, @RequestBody IntraSessionFullDTO dto)
			throws AkdemiaBusinessException, AccessDeniedException {
		return ResponseEntity.ok(intraSessionService.update(dto));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable int id) throws AkdemiaBusinessException, AccessDeniedException {
		intraSessionService.deleteById(id);
		return ResponseEntity.ok().build();
	}
    
	
	

	
	
}