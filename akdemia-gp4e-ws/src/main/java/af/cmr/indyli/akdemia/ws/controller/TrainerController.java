package af.cmr.indyli.akdemia.ws.controller;

import af.cmr.indyli.akdemia.business.dto.full.TrainerFullDTO;
import af.cmr.indyli.akdemia.business.exception.AkdemiaBusinessException;
import af.cmr.indyli.akdemia.business.service.ITrainerService;
import af.cmr.indyli.akdemia.business.utils.ConstsValues;
import af.cmr.indyli.akdemia.ws.utils.AkdemiaUrlBase;
import jakarta.annotation.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.nio.file.AccessDeniedException;
import java.util.List;

/**
 * RESTful controller to manage operations related to employees.
 */
@RestController
@CrossOrigin(origins = AkdemiaUrlBase.url, maxAge = AkdemiaUrlBase.maxAge)
@RequestMapping("/trainers")
public class TrainerController {

	@Resource(name = ConstsValues.ServiceKeys.TRAINER_SERVICE_KEY)
	private ITrainerService trainerService;

	/**
	 * Retrieve the list of all trainers.
	 *
	 * @return ResponseEntity containing the list of trainers.
	 */
	@GetMapping
	public ResponseEntity<List<TrainerFullDTO>> getAll() {
		return ResponseEntity.ok(trainerService.findAllFull());
	}

	/**
	 * Retrieve information about an trainer by their identifier.
	 *
	 * @param id The identifier of the trainer.
	 * @return ResponseEntity containing information about the trainer.
	 * @throws AkdemiaBusinessException If a business exception occurs.
	 */
	@GetMapping("/{id}")
	public ResponseEntity<TrainerFullDTO> getOne(@PathVariable int id) throws AkdemiaBusinessException {
		System.out.println("id " + id);
		return ResponseEntity.ok(trainerService.findById(id));
	}

	/**
	 * Create a new employee.
	 *
	 * @param dto Information about the trainer to create.
	 * @return ResponseEntity containing information about the newly created
	 *         trainer.
	 * @throws AkdemiaBusinessException If a business exception occurs.
	 */
	@PostMapping
	public ResponseEntity<TrainerFullDTO> create(@RequestBody TrainerFullDTO dto) throws AkdemiaBusinessException {
		return ResponseEntity.ok(trainerService.create(dto));
	}

	/**
	 * Update information about an trainer.
	 *
	 * @param id  The identifier of the employee to update.
	 * @param dto The new information about the trainer.
	 * @return ResponseEntity containing the updated information of the trainer.
	 * @throws AccessDeniedException    If access is denied.
	 * @throws AkdemiaBusinessException If a business exception occurs.
	 */
	@PutMapping("/{id}")
	public ResponseEntity<TrainerFullDTO> update(@PathVariable int id, @RequestBody TrainerFullDTO dto)
			throws AccessDeniedException, AkdemiaBusinessException {
		return ResponseEntity.ok(trainerService.update(dto));
	}

	/**
	 * Delete an employee by their identifier.
	 *
	 * @param id The identifier of the trainer to delete.
	 * @return ResponseEntity with an empty body indicating the trainer has been
	 *         successfully deleted.
	 * @throws AkdemiaBusinessException If a business exception occurs.
	 * @throws AccessDeniedException    If access is denied.
	 */
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable int id) throws AkdemiaBusinessException, AccessDeniedException {
		trainerService.deleteById(id);
		return ResponseEntity.ok().build();
	}
}
